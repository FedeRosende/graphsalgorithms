\subsection{Desarrollo}

El algoritmo que resuelve este problema se basa en las proposiciones que se demuestran a continuación.

\hfill

\begin{lema} \label{lema:circuito_agm}
Sea $C$ un circuito simple de un grafo $G$. Si una arista $e$ de $C$ tiene peso mayor al resto de las aristas de $C$, entonces $e$ no está en ningún AGM de $G$.
\end{lema}

\begin{proof}
Supongamos que $e = (u,v)$ está en algún AGM $T$ de $G$.

Sacar $e$ de $T$ lo separa en dos árboles disconexos $T_1$ y $T_2$ con $u$ en uno y $v$ en el otro. Supongamos sin pérdida de generalidad que $u \in V(T_1)$ y $v \in V(T_2)$. Como $C-e$ es un camino simple entre $u$ y $v$, y para ir de $u$ a $v$ hay que pasar de $T_1$ a $T_2$, debe existir alguna arista $f$ de $C-e$ que conecta un vértice de $T_1$ con uno de $T_2$ en $G$. Entonces, puedo reconectar $T_1$ y $T_2$ usando $f$, obteniendo el árbol $T-e+f$, cuyo peso es menor que el de $T$ porque el peso de $f$ es menor que el de $e$.
\end{proof}


\begin{prop} \label{prop:algun_agm_afuera}
Sea $T$ un AGM de un grafo $G$ y $e$ una arista de $G$ que no está en $T$. $e$ está en algún AGM de $G$ (y no en todos) si y sólo si el circuito simple que se forma al agregar $e$ a $T$ tiene una arista $f \neq e$ tal que $peso(f) = peso(e)$.
\end{prop}

\begin{proof}\
\begin{itemize}
\item[$(\implies)$]
Supongamos que ninguna arista $f \neq e$ del circuito simple $C$ de $T+e$ tiene peso igual al de $e$. Si todas estas aristas $f$ tuvieran peso menor al de $e$, entonces, por el lema \ref{lema:circuito_agm}, $e$ no estaría en ningún AGM de $G$ (absurdo). Luego debe existir algún $f$ con peso mayor al de $e$. Pero entonces $T+e-f$ es un árbol generador de $G$ con peso menor al de $T$, que es un AGM (absurdo). Por lo tanto, debe existir alguna arista $f \neq e$ en $C$ tal que $peso(e) = peso(f)$.

\item[$(\impliedby)$]
Si agregamos $e$ a $T$ y sacamos a $f$, se forma el árbol generador $T' = T+e-f$ que tiene el mismo peso que $T$, pues $peso(T') = peso(T) + peso(e) - peso(f) = peso(T)$. Luego $T'$ es un AGM de $G$ que contiene a $e$. Por lo tanto, $e$ está en algún AGM de $G$ (en $T'$) pero no en todos (no en $T$).
\end{itemize}
\end{proof}


\begin{prop} \label{prop:todos_agm}
Sea $T$ un AGM de un grafo $G$ y $e$ una arista de $T$. $e$ está en todos los AGMs de $G$ si y sólo si no existe arista $f$ de $G$ que no está en $T$ tal que $peso(f) = peso(e)$ y el circuito simple que se forma al agregar f a T contiene a $e$.
\end{prop}

\begin{proof}\
\begin{itemize}
\item[$(\implies)$]
Si existiera una tal $f$, $T+f-e$ sería un árbol generador de $G$ de igual peso que $T$. Entonces $T+f-e$ sería un AGM de $G$ que no contiene a $e$ (absurdo).

\item[$(\impliedby)$]
Supongamos que existe algun AGM $T'$ de $G$ que no tiene a $e = (u,v)$. Como $T'$ es conexo, debe haber algún camino simple $P$ en $T'$ entre $u$ y $v$ que no contiene a $e$.
\begin{obs} \label{obs:todos_agm_peso_aristas}
Todas las aristas de $P$ deben tener peso menor o igual al de $e$ ya que, de lo contrario, podría reemplazar una arista que tiene peso mayor al de $e$ por $e$ y obtener un árbol de menor peso.
\end{obs}
Todos los vértices $w \neq u,v$ de $P$ son conexos a $u$ en $T$ por algún camino simple $Q_w$ que puede pasar o no por $e$. Analizamos por casos.

\begin{itemize}
\item \emph{Ningún camino $Q_w$ pasa por $e$.}\\
Sea $f = (x,z)$ la primera arista de $P$ partiendo desde $v$ hacia $u$ que no está en $T$ y sea $R$ este camino que va de $v$ a $z$ y que tiene a $f$ como última (y posiblemente única) arista. $f$ existe porque $T$ no puede contener a P (si no $P+e$ sería un circuito en $T$). Como $Q_z$ va de $z$ a $u$ y no pasa por $e$, al agregar $f$ a $T$ se forma el circuito simple $C = R+Q_z+e$. Si el peso de $f$ fuera menor que el de $e$, el árbol $T+f-e$ tendría menor peso que $T$ (absurdo). Luego $peso(f) = peso(e)$ (por la observación \ref{obs:todos_agm_peso_aristas}) y al agregar $f$ a $T$ se forma un circuito simple que contiene a $e$ ($C$). Esto es absurdo.

\item \emph{Todos los caminos $Q_w$ pasan por $e$.}\\
Ningún camino simple de $w$ a $v$ pasa por $e$, pues si alguno hiciera, debería pasar primero por $u$ para ir por $e$. Pero entonces habría un camino de $w$ a $u$ que no pasa por $e$ (absurdo).
En este caso puedo hacer un argumento simétrico al anterior.

\item \emph{Algunos caminos $Q_w$ pasan por $e$ y otros no.}\\
Debe existir alguna arista $f = (x,z)$ de $P$ que no está en $T$ y tal que el camino simple $Q_x$ de $x$ a $u$ pasa por $e$ y el camino simple $Q_z$ de $z$ a $u$ no pasa por $e$ (hacemos un abuso de notación y decimos que $uu$ es un camino simple de $u$ a $u$ que no pasa por ninguna arista). Si no existiera, dada una arista $f = (x,z)$ de $P$, $f$ estaría en $T$ o bien no estaría en $T$ y $Q_x$ y $Q_z$ pasarían ambos por $e$ o no pasaría ninguno por $e$. Sea entonces $P = f_1 f_2 \dots f_k$ donde $f_i = (x_i,z_i)$, $x_1 = u$, $z_k = v$ y $z_j = x_{j+1} \forall 1 \leq j < k$. Dado $1 \leq i \leq k$, si $Q_{x_i}$ no pasa por $e$ entonces $Q_{z_i}$ tampoco, porque si $f_i$ está en $T$ entonces $Q_{z_i} = Q_{x_i}+f_i$ es un camino de $z_i$ a $u$ que no pasa por $e$, y si $f_i$ no está en $T$, $Q_{z_i}$ no puede pasar por $e$ porque $Q_{x_i}$ no lo hace (notar que esto vale con el abuso de notación mencionado). Como $Q_{x_1} = Q_u$ no pasa por $e$ (por el abuso de notación), inductivamente, ningún camino $Q_w$ para $w \in P$ para por $e$ (absurdo). Luego debe existir el $f$ mencionado.\\
Entonces, si agrego $f$ a $T$ se forma el ciclo $C = Q_x+f+Q_z$ que contiene a $e$ (en $Q_x$). El peso de $f$ no puede ser menor al de $e$ porque si no el árbol $T+f-e$ tendría menor peso que $T$ (absurdo). Luego $peso(f) = peso(e)$ (por la observación \ref{obs:todos_agm_peso_aristas}) y al agregar $f$ a $e$ se forma un circuito simple que contiene a $e$ ($C$). Esto es absurdo.
\end{itemize}

Por lo tanto, $e$ está en todos los AGMs de $G$.
\end{itemize}
\end{proof}


\begin{prop} \label{prop:algun_agm_adentro}
Sea $T$ un AGM de un grafo $G$ y $e$ una arista de $T$. $e$ está en algún AGM de $G$ y no en todos si y sólo si existe alguna arista $f$ de $G$ con peso igual al de $e$ tal que al agregar $e$ a $T$ se forma un circuito simple que contiene a $e$.
\end{prop}

\begin{proof}
Si existe una tal arista $f$, $T' = T+f-e$ es un árbol generador de $G$ con peso igual al de $T$. Entonces $T'$ es un AGM de $G$ que no contiene a $e$. Por lo tanto $e$ está en algún AGM de $G$ (en $T$) pero no en todos (no en $T'$).

Si no existe una tal arista $f$, la proposición \ref{prop:todos_agm} dice que $e$ está en todos los AGMs de $G$.
\end{proof}


\subsubsection{El algoritmo}

A partir de estas proposiciones definimos el siguiente algoritmo para resolver el problema dado un grafo $G$ que lo modela.

\begin{enumerate}
\item \label{enum:obtener_agm} Obtener un AGM cualquiera $T$ de $G$.
\item \label{enum:marcar_algunos} Para cada arista $e$ de $G$ que no está en $T$:
	\begin{enumerate}
	\item Poner la solución de toda arista $f \neq e$ del circuito simple $C$ que se forma en $T+e$ como ``alguna"\ si $peso(f) = peso(e)$.
	\item Si se puso la solución de alguna arista en el punto anterior, poner la solución de $e$ como ``alguna". Si no, ponerla como ``ninguna".
	\end{enumerate}
\item \label{enum:marcar_todos} Poner la solución de todas las aristas que faltan como ``toda".
\end{enumerate}

A continuación describimos con más detalle cómo se llevan a cabo los pasos enunciados antes. Vamos separar el algoritmo (algoritmo \ref{alg:hiperconectados}) en dos partes: una (algoritmo \ref{alg:obtener_agm}) que realizará el paso \ref{enum:obtener_agm} y otra (algoritmo \ref{alg:obtener_soluciones}) que realizará los pasos \ref{enum:marcar_algunos} y \ref{enum:marcar_todos}.

\begin{algorithm}
\caption{Dado un grafo $G$, resuelve el problema Hiperconectados}
\label{alg:hiperconectados}

\begin{algorithmic}[1]
	\Function{Hiperconectados}{$G$}
		\State $(P, A, D) \gets \Call{Obtener-AGM}{G}$
		\State $S \gets \Call{Obtener-Soluciones}{G, P, A, D}$
		\State \Return $S$
	\EndFunction
\end{algorithmic}
\end{algorithm}

El algoritmo \ref{alg:obtener_agm} primero obtiene las aristas de un AGM de $G$ usando alguno de los siguientes algoritmos: Prim, Kruskal o Kruskal con Path Compression. El resto del algoritmo se encarga de definir un árbol enraizado a partir del AGM, obteniendo un diccionario $P$ que indica el vértice predecesor de cada vértice del AGM ($nil$ en el caso de la raíz), un diccionario $A$ que indica la arista que conecta un vértice a su predecesor ($A[v] = (P[v], v) \ \forall v \in V(G)$) y un diccionario $D$ que indica la distancia de cada vértice a la raíz. Esta información es usada posteriormente en el algoritmo \ref{alg:obtener_soluciones} para obtener la solución de cada arista de $G$.

Para determinar el árbol enraizado se usa el algoritmo breadth-first search (BFS) para recorrer todos los vértices del AGM desde un nodo inicial cualquiera (que será la raíz) y cada vez que se descubre un nodo nuevo, en lugar de etiquetarlo, se definen su predecesor, la arista a su predecesor y su distancia al nodo inicial.


\begin{algorithm}
\caption{Dado un grafo $G$, devuelve un diccionario $V(G) \to V(G)$ de predecesores, un diccionario $V(G) \to E(G)$ de aristas al predecesor y un diccionario $V(G) \to \mathbb{N}_0$ de distancias a la raíz que definen un AGM enraizado de $G$.}
\label{alg:obtener_agm}

\begin{algorithmic}[1]
	\Function{Obtener-AGM}{$G$}
		\State Obtener un AGM de $G$ como una lista $L$ de $n-1$ aristas
		\State Crear listas de adyacencias $L_v$ $\forall v \in V(G)$ del AGM a partir de $L$
		\State Crear un diccionario $P$ de $V(G) \to V(G)$
		\State Crear un diccionario $A$ de $V(G) \to E(G)$
		\State Crear un diccionario $D$ de $V(G) \to \mathbb{N}_0$
		\State Crear un diccionario $V$ de $V(G) \to booleano$
		\ForAllDo{$v \in V(G)$}{$V[v] \gets false$}
		\State Crear una cola $C$
		\State Elegir un vértice $v \in V(G)$ cualquiera
		\State \Call{Encolar}{$C, v$}
		\State $P[v] \gets nil$
		\State $A[v] \gets nil$
		\State $D[v] \gets 0$
		\While{$\Call{Tamaño}{C} > 0$}
			\State $u \gets \Call{Desencolar}{C}$
			\State $V[u] \gets true$
			\ForAll{$w \in L_u$ tal que $V[w] = false$}
				\State $\Call{Encolar}{C, w}$
				\State $P[w] \gets u$
				\State $A[w] \gets (u,w)$
				\State $D[w] \gets D[u] + 1$
			\EndFor
		\EndWhile
		\State \Return $(P, A, D)$
	\EndFunction
\end{algorithmic}
\end{algorithm}


En el algoritmo \ref{alg:obtener_soluciones} se comienza poniendo la solución de todas las aristas del AGM como ``toda"\ y luego se procede a realizar lo descripto en el paso \ref{enum:marcar_algunos} del algoritmo \ref{alg:hiperconectados}. Esto no es exactamente lo enunciado en los pasos del algoritmo \ref{alg:hiperconectados} pero es equivalente, ya que así, al finalizar el paso \ref{enum:marcar_algunos}, la soluciones de ``las aristas que faltan"\ ya van a quedar puestas como ``toda".

Para recorrer el circuito simple $C$ que se formaría en el AGM si se agregara una arista $e = (u,v) \in E(G)$ que no está en el AGM se usa la siguiente idea. Los vértices $u$ y $v$ pueden estar en ramas (caminos simples desde la raíz a las hojas) distintas del AGM o en la misma. Si están en la misma, el circuito $C$ consiste de la arista $e$ más la porción de la rama que conecta $u$ con $v$. Luego para recorrer $C-e$ basta tomar el vértice que está a mayor distancia de la raíz y ``subir"\ por la rama hasta encontrarse con el otro.

Si $u$ y $v$ pertenecen a ramas distintas, $C$ está formada por $e$ más las porciones de las ramas de $u$ y $v$ que van desde ellos hasta a el primer predecesor común a ambos. Entonces, $C-e$ se recorre subiendo por ambas ramas hasta llegar a dicho predecesor común.

En base a esto, el algoritmo toma primero entre $u$ y $v$ el vértice que se encuentra a mayor distancia de la raíz y sube por la rama de este hasta llegar al mismo nivel que el otro. Si al terminar de subir se encontró con el otro vértice, entonces $u$ y $v$ están en la misma rama y se recorrió correctamente $C-e$. Si no, los nodos deben estar en ramas distintas. Luego se sube por ambas ramas paralelamente, un nivel en cada paso, hasta llegar a un mismo nodo, que será el primer predecesor en común. Como el algoritmo se mantiene en el mismo nivel en ambas rama, se encontrará siempre con el predecesor en común y, entonces, finalizará correctamente el recorrido de $C-e$.

En el recorrido de $C-e$ se va poniendo la solución de las aristas con peso igual al de $e$ como ``alguna"\ y se lleva la cuenta en una variable $c$ la cantidad de tales aristas. Si al finalizar el recorrido la cuenta es cero, se pone la solución de $e$ como ``ninguna". Si no, se pone como ``alguna". De esta forma se realiza el paso \ref{enum:marcar_algunos} del algoritmo.


\begin{algorithm}
\caption{Dado un grafo $G$ y los diccionarios $P$, $A$ y $D$ que devuelve el algoritmo \ref{alg:obtener_agm}, devuelve un diccionario $S$ que indica si una arista pertenece a alguno, ninguno o todos los AGMs de $G$.}
\label{alg:obtener_soluciones}

\begin{algorithmic}[1]
	\Function{Obtener-Soluciones}{$G, P, A, D$}
		\State Crear un diccionario $S$ de $E(G) \to \{ alguna, ninguna, toda \}$
		\ForAllDo{$e \in A$}{$S[e] \gets toda$}
		\ForAll{$e = (w,z) \in E(G)$ tal que $S[e] = nil$}
			\State $u \gets w$
			\State $v \gets z$
			\IfThen{$D[u] > D[v]$}{Intercambiar $u$ y $v$}
			\State $d_u \gets D[u]$
			\State $d_v \gets D[v]$
			\State $c \gets 0$
			\While{$d_u < d_v$}
				\If{$peso(A[v]) = peso(e)$}
					\State $S[A[v]] \gets alguna$
					\State $c \gets c + 1$
				\EndIf
				\State $v \gets P[v]$
				\State $d_v \gets d_v - 1$
			\EndWhile
			\While{$u \neq v$}
				\If{$peso(A[u]) = peso(e)$}
					\State $S[A[u]] \gets alguna$
					\State $c \gets c + 1$
				\EndIf
				\State $u \gets P[u]$
				\State $d_u \gets d_u - 1$
				\If{$peso(A[v]) = peso(e)$}
					\State $S[A[v]] \gets alguna$
					\State $c \gets c + 1$
				\EndIf
				\State $v \gets P[v]$
				\State $d_v \gets d_v - 1$
			\EndWhile
			\IfThen{$c = 0$}{$S[e] \gets ninguna$}
			\ThenElse{$S[e] \gets alguna$}
		\EndFor
		\State \Return $S$
	\EndFunction
\end{algorithmic}
\end{algorithm}


\subsubsection{Correctitud y complejidad}

La correctitud de los pasos enunciados del algoritmo \ref{alg:hiperconectados} se justifican directamente con las proposiciones probadas. El paso \ref{enum:marcar_algunos} es correcto por las proposiciones \ref{prop:algun_agm_afuera} y \ref{prop:algun_agm_adentro}. En el paso \ref{enum:marcar_todos}, las aristas que faltan son todas las aristas de $T$ (porque en el paso \ref{enum:marcar_algunos} todas las aristas que no están en $T$ fueron puestas como ``alguna"\ o ``ninguna") para las cuales ninguna arista de igual peso forma un circuito simple que las contiene al ser agregada a $T$. Luego, la proposición \ref{prop:todos_agm} garantiza que estas aristas están en todos los AGMs de $G$. Por lo tanto, esta sucesión de pasos es correcta. Queda probar que los algoritmos que la implementan lo hacen correctamente.

En el algoritmo \ref{alg:obtener_agm} se obtienen las aristas de un AGM usando el algoritmo de Prim, Kruskal o Kruskal con Path Compression. La implementación usada de estos algoritmos fue extraída de \cite{cormen}, donde se puede ver la demostración de su correctitud y complejidad. Sólo hacemos las siguientes aclaraciones acerca de la implementación:

\begin{itemize}
\item La implementación del min-priority queue usada para el algoritmo de Prim es la de un binary min-heap, presentado también en \cite{cormen}. Luego, como se prueba allí, la complejidad del algoritmo es de $O(m \log{n})$.
\item La implementación del algoritmo de Kruskal sin Path Compression es igual a la versión que utiliza Path Compression, sólo que en lugar de usar la estructura de datos disjoint-set se usa un diccionario que indica la componente a la que pertenece cada vértice. Luego, en vez de usar la operación \textsc{Find-Set} para obtener la componente de un vértice, simplemente se obtiene el valor del vértice en el diccionario (en $O(1)$); y en lugar de usar \textsc{Union} para unir dos conjuntos, se recorre todo el diccionario (en $O(n)$) cambiando los valores de los vértices correspondientes.\\
Tomando estas nuevas complejidades, se puede ver a partir del desarrollo hecho en \cite{cormen} que la complejidad de este algoritmo es de $O(n^2 + m \log{m}) = O(n^2 \log{n})$, pues $m = O(n^2)$.
\item El algoritmo de Kruskal con Path Compression se implementa usando la estructura disjoint-set presentada también en \cite{cormen}. Luego, como se ve allí, la complejidad es de $O((n+m) \log{m}) = O (m \log{n})$ ya que $m = O(n^2)$ y, como asumimos que $G$ es conexo, $n = O(m)$.
\end{itemize}

Como mencionamos antes, el resto del algoritmo \ref{alg:obtener_agm} recorre el AGM usando BFS y define los valores de los diccionarios en el recorrido. La implementación de BFS fue extraída de \cite{cormen}. Allí se prueba su correctitud y su complejidad de $O(n+m)$.

Como dijimos antes, el algoritmo \ref{alg:obtener_soluciones} pone la solución de todas las aristas del AGM como ``toda"\ primero y luego hace el recorrido de los circuitos. Esto, como también mencionamos, es equivalente a realizar los pasos \ref{enum:marcar_algunos} y \ref{enum:marcar_todos} que enunciamos al principio. Basta probar entonces que las aristas que recorre el algoritmo son efectivamente las de $C-e$. Para ello podemos argumentar que agregar $e = (u,v)$ al AGM formaría un único circuito simple, que por definición es $C$. El algoritmo sube por las ramas desde $u$ y $v$ hasta el primer predecesor $z$ común a $u$ y $v$. Como $z$ es el primer predecesor común y no se repiten nodos en la subida por las ramas, el algoritmo recorre un camino simple de $u$ a $v$ en el AGM: el camino que empieza en $u$, ``sube"\ hasta $z$ y ``baja"\ hasta $v$. Luego, este camino junto con $e$ forma un circuito simple que, como hay uno solo, debe ser $C$.

Una vez demostrado esto, es trivial ver que el algoritmo hace lo que dicen los pasos \ref{enum:marcar_algunos} y \ref{enum:marcar_todos}. Por lo tanto, como ya probamos que dichos pasos son correctos, el algoritmo también lo es.

En cuanto a la complejidad, el algoritmo \ref{alg:obtener_soluciones} recorre todas las aristas y sólo para aquellas que no pertenecen al AGM hace el recorrido por las ramas mencionado anteriormente. Como una rama del árbol tiene altura de a lo sumo $n-1$ y el resto de las operaciones que se hacen tienen costo constante, la complejidad de este algoritmo es de $O(m + (m-n) n) = O(nm)$.

Por lo tanto, el costo del algoritmo es de $O(nm)$ para Prim y Kruskal con path compression y $O(n^2 \log{n} + nm)$ para Kruskal sin path compression.

\subsection{Experimentación}

Con el fin de obtener alguna noción respecto a los beneficios de cada algoritmo, se llevaron a cabo tests y se contrastaron los resultados obtenidos con cada uno.

\subsubsection{Generación de casos}

Para le generación de casos se implementó un programa que genera de forma aleatoria un grafo, dada una cantidad de vértices $n$, una cantidad de aristas $m$ y un intervalo para los pesos $[p_{min}, p_{max}]$.

El programa primero construye un árbol aleatoriamente para garantizar que el grafo generado sea conexo, y luego agrega el resto de las arista hasta llegar la cantidad indicada. Para construir el árbol crea un grafo con $n$ vértices aislados y agrega $n-1$ aristas eligiendo para cada una un vértice que se encuentra en el subárbol generado y otro vértice aislado.

Finalmente, luego de agregar todas las aristas se generan los pesos de las mismas con una distribución uniforme en el rango especificado.

\subsubsection{Resultados}

En primer lugar, se fijó la cantidad de aristas ($m$) variando la cantidad de vértices ($n$).

Para fijar la cantidad de aristas se probaron varios valores distintos y se pudo observar que un valor de 200000 resultaba en tiempos suficientemente grandes para cantidades bajas de vértices que no fueran distorsionados por los errores de medición, y producía tiempos realizables para valores grandes de $n$.

El rango de pesos elegido para las 200000 aristas fue entre 1 y 1000. El objetivo fue que se generen varias aristas con el mismo peso para que el algoritmo pueda encontrarse con todos los casos: aristas que están en todos, ninguno o algún AGM.

La cantidad de vértices se varió de 5000 a 200000, que son aproximadamente las cantidades mínimas y máximas posibles para la cantidad de aristas fijadas. Se midieron los tiempos de ejecución en cada incremento de 5000 de $n$ y para cada $n$ se realizaron 10 corridas sobre las cuales se tomó el promedio.

Para variar la cantidad de aristas se fijó la cantidad de vertices en 500 por las mismas razones que se eligió el valor 200000 para fijar $m$: producía tiempos suficientemente grandes pero realizables. Además, daba lugar a un rango para $m$ amplio como para observar el efecto de la variación. Esta vez, sin embargo, se corrió el programa 50 veces ya que los tiempos eran menores y, por lo tanto, más distorsionables por los errores de medición.

El resultado de variar la cantidad de arista fue el siguiente:

\begin{figure}[H]
\centering
\includegraphics[width=0.65\textwidth]{TiemposAristas.png}
\captionof{figure}{Comparación de los tiempos para los tres algoritmos de AGM}
\end{figure}

\paragraph{Respaldo empírico} \hfill

Por otro lado, se graficaron los cocientes entre los tiempos obtenidos y las cotas de complejidad expuestas previamente para cada algoritmo.

El resultado de variar la cantidad de arista es:

\begin{figure} [H]
\centering
\begin{subfigure}{.36\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{CocientePrimAristas.png}
  \caption{Prim}
\end{subfigure}%
\begin{subfigure}{.36\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{CocienteKruskalAristas.png}
  \caption{Kruskal}
\end{subfigure}%
\begin{subfigure}{.36\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{CocienteKruskalPC.png}
  \caption{Kruskal Path Compression}
\end{subfigure}
\captionof{figure}{Cocientes}
\end{figure}

Con respecto a la cantidad de vértices se tienen los siguientes resultados.

\begin{figure} [H]
\centering
\begin{subfigure}{.36\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{CocientePrimV.png}
  \caption{Prim}
\end{subfigure}%
\begin{subfigure}{.36\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{CocienteKruskalV.png}
  \caption{Kruskal}
\end{subfigure}%
\begin{subfigure}{.36\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{CocienteKruskalPCV.png}
  \caption{Kruskal Path Compression}
\end{subfigure}
\captionof{figure}{Cocientes}
\end{figure}

Se observa que a partir de un punto es posible ajustar la curva obtenida por una constante o el gráfico de la función tiende a cero. Por lo tanto, se tiene evidencia para afirmar que las cotas obtenidas son correctas.


%Variando aristas
