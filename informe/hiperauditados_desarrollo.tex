\subsection{Desarrollo}

Comenzamos esta sección con algunas definiciones. Usamos $n$ y $m$ para referirnos a la cantidad de ciudades y la cantidad de rutas respectivamente. Dadas dos ciudades $u$ y $v$, llamamos $c(u)$ al costo de la ciudad, $peso(u,v)$ al peso o la cantidad de combustible necesaria para ir por la ruta $(u,v)$ y $d(u,v)$ a la cantidad mínima de combustible necesaria para ir de $u$ a $v$ (la longitud del camino mínimo de $u$ a $v$). Denotamos la capacidad máxima del tanque como $C$.

Para resolver este problema desarrollamos un algoritmo de progamación dinámica que se basa en las siguientes proposiciones.

\hfill

\begin{prop} \label{prop:propiedades_paradas}
Sean $a$ y $b$ dos ciudades distintas. Asumiendo que es necesario cargar combustible en alguna ciudad, existe un recorrido óptimo de $a$ a $b$ con secuencia de paradas (ciudades en las que se carga combustible) $u_1 u_2 \cdots u_k \ (k \geq 1)$ tal que:
\begin{enumerate}
\item \label{enum:parada_camino_minimo} Para ir de $u_i$ a $u_{i+1}$ se toma un camino mínimo (que requiere la menor cantidad de combustible).
\item \label{enum:paradas_cargar_justo} Si $c(u_i) \geq c(u_{i+1})$ entonces en $u_i$ no hay suficiente combustible para ir a $u_{i+1}$ y se carga la cantidad de combustible necesaria para llegar a $u_{i+1}$ con el tanque vacío.
\item \label{enum:paradas_cargar_todo} Si $c(u_i) < c(u_{i+1})$ entonces en $u_i$ se llena el tanque.
\item \label{enum:paradas_ultima_parada} En $u_k$ se carga la cantidad de combustible necesaria para llegar a $b$ con el tanque vacío.
\end{enumerate}
\end{prop}

\begin{proof}
Sea $R$ un recorrido óptimo de $a$ a $b$ con secuencia de paradas $u_1 u_2 \cdots u_k$.

\begin{enumerate}
\item
Si para ir de $u_i$ a $u_{i+1}$ no se tomara un camino mínimo, se podría tomar un camino que requiere menos combustible y cargar menos en $u_i$ y/o llegar a $u_{i+1}$ con más combustible y cargar menos en $u_{i+1}$ o en alguna parada posterior. Esto resultaría en un recorrido de $a$ a $b$ de menor costo, algo que es absurdo porque el $R$ es óptimo. Luego todos los recorridos óptimos toman un camino mínimo para ir de $u_i$ a $u_{i+1}$.

\item
Si hubiera suficiente combustible para ir a $u_{i+1}$ se podría ir por el mismo camino a $u_{i+1}$ pero sin cargar en $u_i$ y se podría cargar con a lo sumo el mismo costo en $u_{i+1}$ lo que se carga en $u_i$ en el recorrido $R$. Si $c(u_{i+1}) \geq c(u_{i+2})$ y sigue alcanzando el combustible para ir de $u_{i+1}$ a $u_{i+2}$, puedo hacer el mismo argumento hasta llegar a una parada $u_j$ tal que $c(u_j) \geq c(u_{j+1})$ y no alcanza el combustible para ir a $u_{j+1}$, o bien $c(u_j) < c(u_{j+1})$ o $u_j = u_k$. En cualquiera de esos casos tendría un recorrido que cumple que no alcanza el combustible para ir de una parada a otra de a lo sumo igual costo para el tramo analizado (de $u_i$ a $u_j$) y que es óptimo por tener menos o igual costo que $R$. Como $i$ es cualquiera, probamos que existe un recorrido que cumple con eso en todas las paradas. \\
Supongamos entonces que $R$ cumple con lo que acabamos de probar y que se llega a $u_{i+1}$ con una cantidad $\delta > 0$ de combustible en el tanque. Como en $u_i$ no alcanza el combustible para ir a $u_{i+1}$, lo anterior implica que en $u_i$ se carga lo que falta para ir a $u_{i+1}$ y $\delta$ más. Pero si no se cargara este $\delta$ adicional en $u_i$ y, en su lugar, se cargara en $u_{i+1}$, que tiene a lo sumo el mismo costo, se tendría un recorrido en el que se carga en $u_i$ lo necesario para llegar a $u_{i+1}$ con el tanque vacío que es óptimo porque es a lo sumo tan costoso como $R$.

\item
Supongamos que en $u_i$ no se llena el tanque, sino que se sale de $u_i$ con una cantidad $C - \delta$ de combustible, para algún $\delta > 0$. Llamemos $l$ a la cantidad de combustible que se carga en $u_{i+1}$. Si en $u_i$ se cargara $l$ más de combustible (o a lo sumo $\delta$), en $u_{i+1}$ se podría cargar $l$ (o $\delta$) menos. Pero como el costo del combustible es menor en $u_i$, esto resultaría en un recorrido de $a$ a $b$ con menor costo que $R$. Esto es absurdo porque $R$ es óptimo. Por lo tanto, todos los recorridos óptimos deben llenar el tanque en una parada si el costo allí es menor que en la próxima.

\item
Cuando se llega a $u_k$ no puede haber suficiente combustible para ir a $b$, ya que si lo hubiera se podría ir por el mismo camino a $b$ pero sin cargar en $u_k$ y, entonces, se llegaría a $b$ con un menor costo. \\
Luego, en $u_k$ se debe cargar combustible para ir a $b$. Si no se cargara lo necesario para llegar a $b$ con el tanque vacío se estaría gastando dinero de más y el recorrido no sería óptimo.
\end{enumerate}
\end{proof}

\hfill

En base a estas proposiciones se define la función recursiva $f_v(u, l, k)$, que dada dos ciudades $u$ y $v$, un entero no negativo $l$ y un entero positivo $k$, indica el costo mínimo de ir de $u$ a $v$ empezando con $l$ litros de combustible y pasando por a lo sumo $k$ paradas, asumiendo que se debe cargar combustible en $u$ en un recorrido óptimo.
\begin{align*}
f_v(u, l, k) &=
\begin{cases}
c(u) \cdot (d(u,v) - l) & \text{si } k = 1 \,\land\, l < d(u, v) \leq C \\
\infty & \text{si } k = 1 \,\land\, (l \geq d(u,v) \,\lor\, d(u,v) > C) \\
\min\limits_{\substack{w \,:\, w \neq v \,\land \\ d(u,w) \leq C}} g_v(u, w, l, k) & \text{si } k > 1
\end{cases} \\
\\
g_v(u, w, l, k) &= 
\begin{cases}
f_v(w, l, k-1) & \text{si } w = u \\
c(u) \cdot (d(u,w) - l) + f_v(w, 0, k-1) & \text{si } c(u) \geq c(w) \,\land\, l < d(u,w) \\
c(u) \cdot (C - l) + f_v(w, C - d(u,w), k-1) & \text{si } c(u) < c(w) \\
\infty & \text{si no}
\end{cases}
\end{align*}

Cuando $k = 1$, como asumimos que se debe cargar combustible en $u$, $u$ debe ser la única parada del recorrido. Luego, el costo mínimo será el de cargar lo necesario para llegar a $v$ con el tanque vacío (por \ref{enum:paradas_ultima_parada}). Si esto no es posible, ya sea porque no alcanza la capacidad del tanque para ir a $v$ ($d(u,v) > C$) o porque ya hay suficiente combustible para ir a $v$ y no se puede llegar con el tanque vacío habiendo cargado en $u$ ($l \geq d(u,v)$), entonces decimos que el costo mínimo es $\infty$. Esto nos va a permitir descartar estos casos cuando busquemos recursivamente el costo mínimo.

En el caso general ($k > 1$), debemos comparar por un lado los recorridos que tienen exactamente $k$ paradas (incluyendo a $u$) y, por el otro, los que tienen menos.

Para lo primero, por la proposición \ref{prop:propiedades_paradas}, podemos considerar sólo los recorridos que cumplen con las propiedades que esta enuncia, porque la misma garantiza que existe un recorrido óptimo que cumple dichas propiedades. Es por esto que en $g_v$ se tienen en cuenta los casos en que $c(u) \geq c(w)$ y $c(u) < c(w)$. Cuando $c(u) \geq c(w)$, se pide además que $l < d(u,w)$ porque la proposición \ref{prop:propiedades_paradas} dice que en ese caso existe un recorrido óptimo en el cual no hay suficiente combustible al llegar a $u$ para ir a $w$ (asumiendo que son paradas de un recorrido óptimo). Luego se toma el costo de cargar lo necesario o de llenar el tanque, según el caso, más el costo mínimo de ir de $w$ a $v$ pasando por exactamente $k-1$ paradas, que se obtiene llamando recursivamente a $f_v$ \emph{si no consideramos los demás casos definidos en $g_v$}. No tomamos $w = v$ porque no tiene sentido cargar combustible en la ciudad destino. Cabe aclarar también que en estos casos $w$ no puede ser $u$ ya que tanto $l < d(u,w) = d(u,u) = 0$ como $c(u) < c(w) = c(u)$ son falsos.

Con lo dicho hasta ahora la función $f_v$ indica el costo de un recorrido óptimo que tiene \emph{exactamente} $k$ paradas. Para considerar los que tienen menos de $k$ paradas debemos comparar los casos mencionados antes con $f_v(u, l, i)$ para $i = k-1, k-2, \dots, 1$. En realidad, sólo es necesario compararlos con $f_v(u, l, k-1)$, porque para obtener el valor de este se hará una comparación con $f_v(u, l, k-2)$, y este a su vez se comparará con $f(u, l, k-3)$. Así, recursivamente, se compararán con todos los valores hasta $f_v(u, l, 1)$. Por esta razón, en $g_v$ se define el caso en el que $w = u$.

Es importante observar que con esto último los dos casos mencionados antes ya no indican el costo de un recorrido óptimo de \emph{exactamente} $k$ paradas, sino que es de \emph{a lo sumo} $k$ paradas. De todas formas, el resultado es el mismo ya que un recorrido de exactamente $k$ paradas es un recorrido de a lo sumo $k$ paradas.

Otro detalle a destacar es que cuando $w$ no entra en ninguno de los tres casos mencionados, el valor del costo mínimo es $\infty$. Esto se hace así para descartar los recorridos que no cumplen con las propiedades de la proposición \ref{prop:propiedades_paradas} a la hora de hacer la minimización.

Dicho todo esto, el costo mínimo de un recorrido de $u$ a $v$, como en nuestro problema empezamos con el tanque vacío, es $f_v(u, 0, q)$, donde $q$ es la cantidad máxima de paradas que puede tener un recorrido óptimo que cumple con la proposición \ref{prop:propiedades_paradas}. A continuación demostramos que $q \leq \min\{n, C\} \cdot n$.

\hfill

\begin{lema} \label{lema:cota_litros_en_tanque}
En un recorrido óptimo entre dos ciudades que cumple con lo enunciado en la proposición \ref{prop:propiedades_paradas} se puede llegar a una parada con a lo sumo $\min\{n, C\}$ cantidades distintas de combustible en el tanque.
\end{lema}

\begin{proof} Sea $u_1, u_2, \dots, u_k$ la secuencia de paradas de un recorrido de la forma de la proposición \ref{prop:propiedades_paradas}.

\begin{itemize}
\item
Si $c(u_i) \geq c(u_{i+1})$ se llega a $u_{i+1}$ con el tanque vacío, es decir, una cantidad $0$ de combustible.

\item
Si $c(u_i) < c(u_{i+1})$ se llena el tanque en $u_i$ y, entonces, se llega a $u_{i+1}$ con $C - d(u_i, u_{i+1})$. Luego, como $u_i$ puede ser cualquiera de las $n-1$ ciudades distintas a $u_{i+1}$, se puede llegar a $u_{i+1}$ con a lo sumo $n-1$ cantidades distintas de combustible en el tanque.
\end{itemize}

Además, como buscamos recorridos que empiezan con el tanque vacío y a la ciudad destino se llega sin combustible, las únicas cantidades de combustible posibles para una ciudad $v$ son las mencionadas antes: $0$ y $C - d(u,v) \ \forall\ u \in V(G) \setminus \{v\}$, que son a lo sumo $n$ valores distintos. Pero el tanque tiene una capacidad máxima $C$, luego se puede llegar a $v$ con a lo sumo $\min\{n, C\}$ cantidades distintas de combustible en el tanque.

\end{proof}

\begin{prop} \label{prop:cota_paradas}
Un recorrido óptimo entre dos ciudades que cumple con lo enunciado en la proposición \ref{prop:propiedades_paradas} hace a lo sumo $\min\{n, C\} \cdot n$ paradas.
\end{prop}

\begin{proof}
Sea $R$ un recorrido óptimo que cumple con lo enunciado en la proposición \ref{prop:propiedades_paradas}.

Para encontrar el costo mínimo de un recorrido óptimo entre las dos ciudades podemos construir un grafo $H$ que cumple con lo siguiente:

\begin{itemize}
\item Los vértices de $H$ son todas las tuplas posibles $(u, l)$ donde $u$ es un vértice de $G$ y $l$ es una cantidad posible de combustible con la que se puede llegar a $u$.
\item Dado un vértice $(u, l)$ de $H$ y un vértice $v$ de $G$ tal que $d(u,v) \leq C$, si $c(u) \geq c(v)$ y $l < d(u,v)$ entonces existe un arco de $(u, l)$ a $(v, 0)$ de peso $c(u) \cdot (d(u,v) - l)$; si $c(u) < c(v)$ entonces existe un arco de $(u, l)$ a $(v, C - d(u,v))$ de peso $c(u) \cdot (C - l)$
\end{itemize}

Este grafo es simplemente una representación de todos los $R$ posibles. Es fácil ver que un camino entre un vértice $(u, 0)$ y otro $(v, 0)$ en $H$ se corresponde con un posible $R$, que la longitud de tal camino es el costo de $R$ y que en $H$ se encuentran todos los posibles $R$. Luego, un recorrido óptimo que cumple con lo enunciado en la proposición \ref{prop:propiedades_paradas} debe ser un camino mínimo en $H$. Como todo camino mínimo en $H$ tiene a lo sumo $V(H)$ vértices y, por el lema \ref{lema:cota_litros_en_tanque}, hay a lo sumo $\min\{n, C\} \cdot n$ vértices en $H$, todo recorrido que cumple con lo enunciado en la proposición \ref{prop:propiedades_paradas} debe tener a lo sumo $\min\{n, C\} \cdot n$ paradas.

\end{proof}

\begin{obs}
Generar el grafo de la demostración de la proposición \ref{prop:cota_paradas} y luego hallar la longitud de un camino mínimo es una solución posible del problema. Sin embargo, decidimos usar el algoritmo de programación dinámica ya que tiene una complejidad temporal y espacial menor.
\end{obs}

\hfill

\begin{obs}
La cota de la cantidad de paradas de la proposición \ref{prop:cota_paradas} es la mejor que se logró demostrar. Sin embargo, creemos que esta no es buena. El estudio de varios casos particulares nos llevan a conjeturar que en un recorrido óptimo ninguna parada se repite más de dos veces. En tal caso la cota sería de $2n$.
\end{obs}


\subsubsection{El algoritmo}

Para resolver el problema, dado un grafo en el cual los vértices representan ciudades y las aristas representan las rutas (donde el peso es la cantidad de litros de combustibles), el algoritmo \ref{alg:hiperauditados} obtiene la cantidad mínima de combustible necesaria para ir entre cada par de ciudades y luego usa el algoritmo de programación dinámica basado en la función $f$ descrita antes para obtener los costos mínimos.

\begin{algorithm}
\caption{Dado un grafo $G$ que modela las ciudades y las rutas, resuelve el problema Hiperauditados}
\label{alg:hiperauditados}

\begin{algorithmic}[1]
	\Function{Hiperauditados}{$G$}
		\State Crear un diccionario $D$ de $V(G) \times V(G) \to \mathbb{N}$
		\State Obtener las longitudes de los caminos mínimos de $G$ y guardarlos en $D$
		\State Crear un diccionario $S$ de $V(G) \times V(G) \to \mathbb{N}$
		\ForAll{$v \in V(G)$}
			\State Crear un diccionario $M$ de $V(G) \times \mathbb{N} \times \mathbb{N} \to \mathbb{N}$ para \emph{memoization}
			\ForAll{$u \in V(G)$ tal que $u < v$}
				\State $S[u,v] \gets \Call{Costo-Minimo}{M, G, D, u, v, 0, \min\{n, C\} \cdot n}$
			\EndFor
		\EndFor
	\EndFunction
\end{algorithmic}
\end{algorithm}

Para obtener los caminos minimos se usan los algoritmos de camino mínimo de Dijkstra, Dijkstra con priority queue, A*, Bellman-Ford, Floyd-Warshall o Dantzig. Los algoritmos de tipo \emph{uno a uno} (sólo A*) se usan $n^2$ veces, una vez para cada par de ciudades posible; los de tipo \emph{uno a todos} (Dijkstra, Dijkstra con priority queue, Bellman-Ford) se aplican $n$ veces, una por cada ciudad de origen; y los de tipo \emph{todos a todos} (Floyd-Warshall, Dantzig) sólo se usan una vez.

El algoritmo \ref{alg:hiperauditados_pd} se traduce directamente de la función $f_v$ descrita anteriormente. Es una implementación de programación dinámica que usa memoization para guardar los resultados computados.

\begin{algorithm}
\caption{Dado un diccionario $M$ de memoization, un grafo $G$, un diccionario $D$ de caminos mínimos de $G$, un vértice origen $u$, un vértice destino $v$, una cantidad de combustible $l$, una cantidad máxima de paradas $k$, guarda en $M$ y devuelve $f_v(u, l, k)$}
\label{alg:hiperauditados_pd}

\begin{algorithmic}[1]
	\Function{Costo-Minimo}{$M, G, D, u, v, l, k$}
		\IfThen{$M[u, l, k] \neq nil$}{\Return $M[u, l, k]$}
		\State $x \gets \infty$
		\If{$k = 1$}
			\IfThen{$l < D[u,v] \leq C$}{$x \gets c(u) \cdot (D[u,v] - l)$}
		\Else
			\ForAll{$w \in V(G)$ tal que $w \neq v \,\land\, D[u,w] \leq C$}
				\If{$w = u$}
					\State $y \gets \Call{Costo-Minimo}{M, G, D, w, v, l, k-1}$
				\ElsIf{$c(u) \geq c(w) \,\land\, l < D[u,w]$}
					\State $y \gets c(u) \cdot (D[u,w] - l) + \Call{Costo-Minimo}{M, G, D, w, v, 0, k-1}$
				\ElsIf{$c(u) < c(w)$}
					\State $y \gets c(u) \cdot (C - l) + \Call{Costo-Minimo}{M, G, D, w, v, C - D[u,w], k-1}$
				\Else
					\State $y \gets \infty$
				\EndIf
				\State $x \gets \min\{x, y\}$
			\EndFor
		\EndIf
		\State $M[u, l, k] \gets x$
		\State \Return $x$
	\EndFunction
\end{algorithmic}
\end{algorithm}


\subsubsection{Correctitud y complejidad}

Los algoritmos de camino mínimo de Dijkstra, Dijkstra con priority queue, Bellman-Ford y Floyd-Warshall fueron extraídos de \cite{cormen}. Allí se demuestra su correctitud y su complejidad de $O(n^2 + m)$, $O(m \log{n})$, $O(nm)$ y $O(n^3)$ respectivamente.

El algoritmo de Dantzig fue extraído de \cite{teorica}. Se puede ver trivialmente del pseudocódigo dado allí que tiene una complejidad de $O(n^3)$ ya que tiene tres ciclos anidados que hacen operaciones de costo 
constante por $O(n)$ iteraciones cada uno.

Para el algoritmo de A* se definen dos funciones heurísticas, una $h_T$ para cuando el grafo cumple con la desigualdad triangular y otra genérica $h_G$ para cuando no lo cumple. Se definen a continuación:
\begin{align*}
h_T(u,v) &=
\begin{cases}
0 & \text{si } u = v \\
\min\limits_{(u,w) \in E(G)} peso(u,w) + \min\limits_{(w,v) \in E(G)} peso(w,v) & \text{si } u \neq v
\end{cases} \\
h_G(u,v) &= 
\begin{cases}
0 & \text{si } u = v \\
\min\limits_{(w,v) \in E(G)} peso(w,v) & \text{si } u \neq v
\end{cases}
\end{align*}

Para obtener los valores que definen a estas funciones simplemente se recorren todas las aristas del grafo guardando los pesos mínimos de las aristas de salida y/o entrada para cada vértice. Esto se puede hacer en $O(m)$.

Coloquialmente, si $u \neq v$, $h_T(u,v)$ es la suma de los pesos de la arista de menor peso que sale de $u$ y la arista de menor peso que entra a $v$, y $h_G(u,v)$ es sólo el peso de la arista de menor peso que entra a $v$. $h_T(u,v)$ es \emph{admisible} porque cualquier camino de $u$ a $v$ debe ir por una arista que sale de $u$ y otra que entra a $v$, y como tomamos la suma de los mínimos pesos de tales aristas, el valor de la función es una subestimación del valor real. Dado que $h_G \leq h_T$, $h_G$ también es una subestimación y, entonces, es admisible. La \emph{consistencia} de ambas funciones se deduce de que
\begin{align*}
h_G(u,v) \leq h_T(u,v) \leq peso(u,z) + \min\limits_{(w,v) \in E(G)} peso(w,v) \leq peso(u,z) + h(z,v)
\end{align*}

Como las funciones heurísticas son consistentes, se puede implementar A* con un algoritmo igual al de Dijkstra pero con un criterio de ordenamiento distinto a la hora de extraer el vértice de mínima distancia estimada (ordenado por $peso(u,v) + h_T(u,v)$ en lugar de $peso(u,v)$). La demostración de esto y otros detalles del algoritmo se pueden ver en \cite{a_estrella}. Decidimos usar la versión del algoritmo de Dijkstra que utiliza un priority queue para implementar A*. Como nuestra función heurística se la puede evaluar con costo constante (una vez definida), la complejidad del algoritmo A* en sí es igual a la de Dijkstra con priority queue ($O(m \log{n})$).

Como dijimos en la descripción del algoritmo \ref{alg:hiperauditados}, los algoritmos de tipo uno-a-uno se usan $n^2$ veces, los de uno-a-todos $n$ veces y los de todos-a-todos una vez. Luego, el paso de obtención de las longitudes de los caminos mínimos de $G$ tiene una complejidad de $O(n^3 + nm) = O(n^3)$ para Dijkstra, $O(m n \log{n})$ para Dijkstra con priority queue, $O(m n^2 \log{n})$ para A*, $O(n^2 m)$ para Bellman-Ford y $O(n^3)$ para Floyd-Warshall y Dantzig.

Luego de obtener las longitudes de los caminos mínimos se obtiene el costo mínimo para cada par de ciudades usando el algoritmo \ref{alg:hiperauditados_pd}, que es una implementación directa de programación dinámica con memoization de la función $f_v$ descrita anteriormente y, por ende, es trivial ver que es correcta.

Una cosa a mencionar de este paso es que en cada iteración, dado un vértice $v$ fijo se obtiene $S[u,v]$ para el resto de los vértices $u$ y no al revés (dado $u$ fijo obtener $S[u,v]$ para $v$ variable). Esto se hace para que todas las llamadas a la función de programación dinámica puedan compartir un mismo diccionario de memoization y, así, usar los resultados computados por llamadas anteriores. De esta forma se aprovecha al máximo el memoization, compartiendo resultados no sólo dentro de una misma llamada sino que también a través de llamadas con el mismo $v$.

Con esta optimización no sólo se hace un algoritmo que es más rápido en la práctica, sino que también nos permite dar una mejor cota de complejidad ya que ahora podemos afirmar que el costo de todos los llamados a la función de programación dinámica en conjunto, dado un $v$ fijo, es el costo de completar el diccionario de memoization, mientras que antes sólo podíamos decir esto para un llamado individual.

El dominio del diccionario $M$ de memoization es $V(G) \times \mathbb{N} \times \mathbb{N}$, donde la segunda componente corresponde a la cantidad de combustible (a lo sumo $q = \min\{n, C\}$ valores) y la última componente a la cantidad de paradas (a lo sumo $qn$). Este diccionario se puede implementar como una matriz de tres dimensiones usando arreglos, lo que resulta en un costo de búsqueda e inserción de $O(1)$. Como hay a lo sumo $n \cdot q \cdot qn$ entradas en $D$ y el algoritmo \ref{alg:hiperauditados_pd} hace a lo sumo $n$ llamados recursivos, el costo de completar el diccionario es de $O(n^3 \cdot q^2)$. Como completamos un diccionario por cada vértice, el costo de obtener los costos mínimos para todos los pares de vértices es $O(n^4 \cdot q^2)$.

La complejidad de obtener los costos mínimos de los recorridos es superior a la de obtener la longitud de los caminos mínimos para cualquier algoritmo de camino mínimo. Por lo tanto, el costo del algoritmo \ref{alg:hiperauditados} es de $O(n^4 \cdot q^2)$. Si la capacidad del tanque es constante esto se puede simplificar a $O(n^4)$.