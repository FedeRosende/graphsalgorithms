#include <algorithm>
#include <iostream>
#include <iterator>
#include <list>
#include <map>
#include <random>
#include <vector>
#include <unistd.h>
#include "cronometro.hpp"
#include "grafo.hpp"

using namespace std;


int main(int argc, char *const *argv) {

	/*
	 * Parseo de argumentos
	 */

	int arg_algoritmo = -1;
	bool arg_tiempos = false;

{
	opterr = 0; // evitar que getopt imprima mensajes de error
	bool error = false;
	int c;

	while ((c = getopt(argc, argv, "a:t")) != -1) {
		switch (c) {
			case 't':
				arg_tiempos = true;
				break;

			case 'a':
				try { arg_algoritmo = stoi(optarg); }
				catch (...) { error = true; }
				if (!error && (0 > arg_algoritmo || arg_algoritmo > 2))
					error = true;
				break;

			default:
				error = true;
		}
	}

	if (error || arg_algoritmo == -1) {
		cerr << "uso: " << argv[0] << " -a algoritmo [-t]\n";
		cerr << "    " << "-a algortimo: algoritmo a usar\n";
		cerr << "    " << "           " << "0  Prim\n";
		cerr << "    " << "           " << "1  Kruskal\n";
		cerr << "    " << "           " << "2  Kruskal con Path Compression\n";
		cerr << "    " << "-t: imprimir tiempo de ejecucion (en milisegundos) "
		               << "por error estandar";
		cerr << endl;
		return 1;
	}
}


	/*
	 * Lectura de datos por entrada estandar
	 */

	// Leo n y m
	size_t n, m;
	cin >> n;
	cin >> m;

	// Leo las aristas
	struct arista { unsigned int u, v, p; };
	vector<arista> aristas(m);

	for (unsigned int i = 0; i < m; ++i) {
		cin >> aristas[i].u;
		cin >> aristas[i].v;
		cin >> aristas[i].p;
	}


	/*
	 * Resolucion del problema
	 */

	// Creo un cronometro y empiezo a medir el tiempo
	Cronometro crono;
	crono.iniciar();

	// Creo un diccionario para guardar los pesos de las aristas
	vector<unsigned int> pesos_aristas(m);

	// Creo el grafo
	Grafo G(n);
	for (auto &a : aristas)
		pesos_aristas[G.agregar_arista_rapido(a.u, a.v)] = a.p;

	// Obtengo un AGM
	list<Grafo::id_arista> aristas_agm;
	switch (arg_algoritmo) {
		case 0:
			G.agm_prim(pesos_aristas, back_inserter(aristas_agm));
			break;
		case 1:
			G.agm_kruskal(pesos_aristas, back_inserter(aristas_agm));
			break;
		case 2:
			G.agm_kruskal_path_comp(pesos_aristas, back_inserter(aristas_agm));
			break;
	}

	// Dado un vertice cualquiera, obtengo los predecesores de los vertices en
	// el arbol, la arista al predecesor y la distancia a la raiz.
	// Hago esto recorriendo el arbol con BFS.
	vector<Grafo::id_vertice> padres(n);
	vector<Grafo::id_arista> aristas_arbol(n);
	vector<unsigned int> distancias(n);

{
	// Elijo un vertice cualquiera
	random_device rd; //seeder
	default_random_engine gen(rd());
	uniform_int_distribution<> unif(0, n-1);
	Grafo::id_vertice v = unif(gen);

	// Creo listas de adyacencias del AGM
	vector<list<Grafo::id_arista>> listas(n);
	for (auto &id : aristas_agm) {
		auto &a = G.arista(id);
		listas[a.vertices.first].push_back(a.id);
		listas[a.vertices.second].push_back(a.id);
	}

	// Creo un mapa que indica si un vertice fue visitado
	std::vector<bool> visitado(n);

	// Creo una cola de vertices a visitar
	list<Grafo::id_vertice> cola;

	// Agrego la raiz a la cola y seteo sus datos
	cola.push_back(v);
	padres[v] = v;
	aristas_arbol[v] = m; // id de arista invalido
	distancias[v] = 0;

	// Recorro el arbol haciendo BFS
	while (cola.size() > 0) {
		// Visito al siguiente vertice
		auto u = cola.front();
		cola.pop_front();
		visitado[u] = true;

		// Recorro los vertices adyacentes no visitados
		for (auto &id_a : listas[u]) {
			auto w = G.adyacente(u, G.arista(id_a));

			if (!visitado[w]) {
				// Lo agrego a la cola
				cola.push_back(w);
				// Seteo el padre, la arista y la distancia
				padres[w] = u;
				aristas_arbol[w] = id_a;
				distancias[w] = distancias[u] + 1;
			}
		}
	}
}

	// Creo un mapa de soluciones de las aristas
	enum solucion { alguna, ninguna, toda, nil };
	vector<Grafo::id_arista> soluciones(m, nil);

	// Pongo "toda" como solucion provisoria de las aristas del AGM
	for (size_t id : aristas_agm) soluciones[id] = toda;

	// Obtengo la solucion de todas las aristas
	for (auto its = G.aristas(); its.first != its.second; ++its.first) {
		auto &a = *its.first;

		// Solo proceso aristas que no estan en el AGM
		if (soluciones[a.id] != nil) continue;

		// Recorro el circuito simple que se forma al agregar la arista al AGM
		// buscando aristas que tengan el mismo peso
		unsigned int peso = pesos_aristas[a.id];
		auto u = a.vertices.first;
		auto v = a.vertices.second;

		// Guardo la cuenta de la cantidad de aristas del mismo peso
		unsigned int cuenta = 0;

		// Dejo en u el vertice de menor distancia a la raiz
		if (distancias[u] > distancias[v]) swap(u, v);
		unsigned int dist_u = distancias[u];
		unsigned int dist_v = distancias[v];

		// Subo el vertice de mayor distancia hasta llegar a la misma distancia
		// que el otro
		while (dist_v > dist_u) {
			if (pesos_aristas[aristas_arbol[v]] == peso) {
				soluciones[aristas_arbol[v]] = alguna;
				++cuenta;
			}
			v = padres[v]; --dist_v;
		}

		// Subo ambos vertices al mismo tiempo hasta que se encuentren
		while (u != v) {
			if (pesos_aristas[aristas_arbol[u]] == peso) {
				soluciones[aristas_arbol[u]] = alguna;
				++cuenta;
			}
			u = padres[u]; --dist_u;

			if (pesos_aristas[aristas_arbol[v]] == peso) {
				soluciones[aristas_arbol[v]] = alguna;
				++cuenta;
			}
			v = padres[v]; --dist_v;
		}

		// Si no encontre ninguna arista del mismo peso, la arista no esta en
		// ningun AGM. Si encontre alguna, esta en algun AGM pero no en todos.
		soluciones[a.id] = cuenta == 0 ? ninguna : alguna;
	}

	// Miro el cronometro
	auto tiempo = crono.mirar();


	/*
	 * Impresion de la solucion
	 */

	const char *strings[] = {"alguna", "ninguna", "toda"};
	for (auto &s : soluciones) cout << strings[s] << endl;

	if (arg_tiempos) cerr << tiempo << endl;


	return 0;
}