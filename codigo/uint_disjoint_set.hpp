#ifndef __UINT_DISJOINT_SET__
#define __UINT_DISJOINT_SET__

#include <algorithm>

using namespace std;


class uint_disjoint_set {

	public:

		uint_disjoint_set(const unsigned int n) : _size(n), _sets(n) {
			_padres = new unsigned int[n];
			_rangos = new unsigned int[n];
			for (unsigned int i = 0; i < n; ++i) {
				_padres[i] = i;
				_rangos[i] = 1;
			}
		}

		uint_disjoint_set(const uint_disjoint_set &otro) :
		_size(otro._size), _sets(otro._sets) {

			_padres = new unsigned int[_size];
			_rangos = new unsigned int[_size];
			for (unsigned int i = 0; i < _size; ++i) {
				_padres[i] = otro._padres[i];
				_rangos[i] = otro._rangos[i];
			}
		}

		void swap(uint_disjoint_set &otro) {
			std::swap(_size, otro._size);
			std::swap(_sets, otro._sets);
			std::swap(_padres, otro._padres);
			std::swap(_rangos, otro._rangos);
		}

		uint_disjoint_set& operator=(uint_disjoint_set otro) {
			swap(otro);
			return *this;
		}

		~uint_disjoint_set() {
			delete[] _padres;
			delete[] _rangos;
		}

		unsigned int size() { return _size; }
		unsigned int sets() { return _sets; }

		unsigned int find(const unsigned int i) {
			// Si no es la raiz hago que su padre sea la raiz
			if (_padres[i] != i) _padres[i] = find(_padres[i]);

			// Devuelvo la raiz
			return _padres[i];
		}

		void merge(const unsigned int i, const unsigned int j) {
			// Obtengo los padres
			unsigned int x = find(i);
			unsigned int y = find(j);

			// Dejo en x el elemento de mayor rango
			if (_rangos[x] < _rangos[y]) std::swap(x, y);

			// Si tienen el mismo rango lo incremento en uno
			else if (_rangos[x] == _rangos[y]) ++_rangos[x];

			// Cuelgo el arbol de y al arbol de x
			_padres[y] = x;

			// Decremento la cantidad de conjuntos disjuntos
			--_sets;
		}


	private:

		unsigned int _size;
		unsigned int _sets;
		unsigned int *_padres;
		unsigned int *_rangos;

};

#endif