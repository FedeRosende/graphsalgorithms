#!/bin/bash

program="$1"
p_min="$2"
p_max="$3"
v_min="$4"
v_max="$5"
reps="$6"

n0=30
m=150
step=2

for n in $(seq $n0 $step $m)
do
	for r in $(seq 1 $reps)
	do
		bash -c "$program -n $n -m $m -p $p_min -q $p_max -v $v_min -w $v_max > datos/test$r-n$n-m$m-p$p_min-q$p_max-v$v_min-w$v_max.in"
	done
done
