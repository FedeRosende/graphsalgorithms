#!/bin/bash

program="$1"
p_min="$2"
p_max="$3"
v_min="$4"
v_max="$5"
rep="$6"
corridas="$7"

n0=30
m=150
step=2

echo "algoritmo,test,n,tiempo"

for alg in {0..5}
do
	for n in $(seq $n0 $step $m)
	do
		for i in $(seq 1 $corridas)
		do
			echo -n "$alg,$rep,$n,"
			bash -c "$program -a $alg -t < datos/test$rep-n$n-m$m-p$p_min-q$p_max-v$v_min-w$v_max.in" 2>&1 1>/dev/null
		done
	done
done
