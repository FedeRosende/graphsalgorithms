#!/bin/bash

program="$1"
n="$2"
p_min="$3"
p_max="$4"
v_min="$5"
v_max="$6"
reps="$7"

m_min=$n
m_max=$(( n * (n-1) / 2 ))
iter=50
step=$(( (n-2) * (n-1) / (2 * (iter-1)) ))
step=$(( step > 0 ? step : 1 ))

for m in $(seq $m_min $step $m_max)
do
	for r in $(seq 1 $reps)
	do
		bash -c "$program -n $n -m $m -p $p_min -q $p_max -v $v_min -w $v_max > datos/test$r-n$n-m$m-p$p_min-q$p_max-v$v_min-w$v_max.in"
	done
done
