#include <iostream>
#include <stdlib.h>
#include <vector>
using namespace std;

void tests(int n, int m, vector<vector<int> > &edges, vector<int> &neighbours, vector<int> &visited);
void treepify(int n, int &m, vector<vector<int> > &edges, vector<int> &visited, vector<int> &neighbours);
void prt(vector<int> &v);
void show(vector<vector<int> > &V);
