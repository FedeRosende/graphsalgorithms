#include "tests.h"
#include <vector>


int main(){
  int m;
  int n = 50;
  while(n>1) {
    vector<int> visited(n+1);
    int lotes = n*n;
    while(lotes>0){
      int limSup = n*(n-1)/2;
      int limInf = n-1;
      m = limInf + rand() % (limSup+1 - limInf);
      vector<vector <int> > edges(n+1, vector<int>(n+1, 0));
      vector<int> neighbours(n+1, 0);
      visited.assign(visited.size(),0);
      tests(n, m, edges, neighbours, visited);
      lotes--;
    }
    n--;
  }
  cout << 0 << endl;
  return 0;
}

void show(vector<vector<int> > &V){
  int j = 1;
  cout << "       ";
  while(j<V.size()){
    cout << " [" << j << "]";
    j++;
  }
  cout << endl;
  j = 1;
  while(j<V.size()){
    cout << "[" << j << "]" << ": ";
    prt(V[j]);
    j++;
  }
}

void prt(vector<int> &v){
  unsigned long tam = v.size();
  unsigned long i = 0;
  while(i<tam){
    cout << v[i];
    if(i<tam -1){cout<< " - ";}
    i++;
  }
  cout << "" << endl;
}


void tests(int n, int m, vector<vector<int> >& edges, vector<int> &neighbours, vector<int> &visited){
  int node1, node2, weight;
  cout << n;
  cout << " " << m << endl;
  treepify(n, m, edges, visited, neighbours);
  while(m>0){
    node1 = 1 + rand() % (n);
    while(neighbours[node1] == (n-1)){
      node1 = 1 + rand() % (n);
    }
    node2 = 1 + rand() % (n);
    weight = rand() % 15;
    //cout << "m = " << m << endl;

    while (edges[node1][node2] != 0 || edges[node2][node1] != 0 || node1 == node2 || neighbours[node2] == (n-1)){
      node2 = 1 + rand() % (n);
    }
    edges[node1][node2] = 1;
    edges[node2][node1] = 1;
    neighbours[node1]++;
    neighbours[node2]++;
    cout << node1 << " ";
    cout << node2 << " ";
    cout << weight << endl;
    m--;
  }
}


void treepify(int n, int &m, vector<vector<int> > &edges, vector<int> &visited, vector<int> &neighbours){
  int node = 1;
  visited[node] = 1;
  int node2 = 1;
  int weight;
  int i = 0;
  while(i<n-1){
    node = node2;
    node2 = rand() % (n) + 1;
    while(visited[node2] != 0){
      node2 = rand() % (n) + 1;
    }
    edges[node][node2] = 1;
    edges[node2][node] = 1;
    visited[node2] = 1;
    weight = rand() % 15;
    neighbours[node]++;
    neighbours[node2]++;
    cout << node << " ";
    cout << node2 << " ";
    cout << weight << endl;
    i++;
    m--;
  }
}
