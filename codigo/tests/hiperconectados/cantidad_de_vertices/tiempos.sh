#!/bin/bash

program="$1"
m="$2"
p_min="$3"
p_max="$4"
corridas="$5"

n0=5000
m=200000
step=5000

echo "algoritmo,n,tiempo"

for alg in {0..2}
do
	for n in $(seq $n0 $step $m)
	do
		for i in $(seq 1 $corridas)
		do
			echo -n "$alg,$n,"
			bash -c "$program -a $alg -t < datos/test-n$n-m$m-p$p_min-q$p_max.in" 2>&1 1>/dev/null
		done
	done
done
