#!/bin/bash

program="$1"
p_min="$2"
p_max="$3"

n0=5000
m=200000
step=5000

for n in $(seq $n0 $step $m)
do
	bash -c "$program -n $n -m $m -p $p_min -q $p_max > datos/test-n$n-m$m-p$p_min-q$p_max.in"
done
