#!/bin/bash

program="$1"
n="$2"
p_min="$3"
p_max="$4"
corridas="$5"

m_min=$n
m_max=$(( n * (n-1) / 2 ))
iter=50
step=$(( (n-2) * (n-1) / (2 * (iter-1)) ))
step=$(( step > 0 ? step : 1 ))

echo "algoritmo,m,tiempo"

for alg in {0..2}
do
	for m in $(seq $m_min $step $m_max)
	do
		for i in $(seq 1 $corridas)
		do
			echo -n "$alg,$m,"
			bash -c "$program -a $alg -t < datos/test-n$n-m$m-p$p_min-q$p_max.in" 2>&1 1>/dev/null
		done
	done
done
