#!/bin/bash

program="$1"
n="$2"
p_min="$3"
p_max="$4"

m_min=$n
m_max=$(( n * (n-1) / 2 ))
iter=50
step=$(( (n-2) * (n-1) / (2 * (iter-1)) ))
step=$(( step > 0 ? step : 1 ))
#step=$(( n - 1 + (n % 2) )) # n-1 si n es par, n si n es impar

for m in $(seq $m_min $step $m_max)
do
	bash -c "$program -n $n -m $m -p $p_min -q $p_max > datos/test-n$n-m$m-p$p_min-q$p_max.in"
done
