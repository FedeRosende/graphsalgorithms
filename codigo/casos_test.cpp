#include <cassert>
#include <iterator>
#include <iostream>
#include <random>
#include <unistd.h>
#include "grafo.hpp"

using namespace std;


int main(int argc, char *const *argv) {

	// Inicializo un seeder
	random_device rd;


	/*
	 * Parseo de argumentos
	 */

	unsigned int arg_n = UINT_MAX;
	unsigned int arg_m = UINT_MAX;
	unsigned int arg_p_min = UINT_MAX;
	unsigned int arg_p_max = UINT_MAX;
	unsigned int arg_v_min = UINT_MAX;
	unsigned int arg_v_max = UINT_MAX;
	bool arg_desig_trian = false;
	bool arg_valores = false;

{
	opterr = 0; // evitar que getopt imprima mensajes de error
	bool error = false;
	int c;

	while ((c = getopt(argc, argv, "n:m:p:q:dv:w:")) != -1) {
		switch (c) {
			case 'n':
				try { arg_n = stoi(optarg); }
				catch (...) { error = true; }
				break;

			case 'm':
				try { arg_m = stoi(optarg); }
				catch (...) { error = true; }
				break;

			case 'p':
				try { arg_p_min = stoi(optarg); }
				catch (...) { error = true; }
				break;

			case 'q':
				try { arg_p_max = stoi(optarg); }
				catch (...) { error = true; }
				break;

			case 'v':
				try { arg_v_min = stoi(optarg); }
				catch (...) { error = true; }
				break;

			case 'w':
				try { arg_v_max = stoi(optarg); }
				catch (...) { error = true; }
				break;

			case 'd':
				arg_desig_trian = true;
				break;

			default:
				error = true;
		}
	}

	if (arg_n == UINT_MAX || arg_m == UINT_MAX ||
	    arg_p_min == UINT_MAX || arg_p_max == UINT_MAX)
		error = true;

	if ((arg_v_min == UINT_MAX && arg_v_max != UINT_MAX) ||
	    (arg_v_min != UINT_MAX && arg_v_max == UINT_MAX))
	    error = true;

	arg_valores = arg_v_min != UINT_MAX;

	if (!error) {
		if (arg_n < 2) error = true;
		if (arg_m < arg_n - 1 || arg_m > arg_n * (arg_n-1) / 2) error = true;
		if (arg_p_min > arg_p_max) error = true;
		if (arg_valores && arg_v_min > arg_v_max) error = true;
	}

	if (error) {
		cerr << "uso: " << argv[0] << " -n cant_vertices -m cant_aristas"
		     << "-p p_min -q p_max [-d] [-v v_min -w v_max]\n";
		cerr << "    " << "-n cant_vertices: cantidad de vertices a generar. "
		               << "Debe ser mayor a 1.\n";
		cerr << "    " << "-m cant_aristas: cantidad de aristas a generar. "
		               << "Debe estar entre n-1 y n*(n-1)/2.\n";
		cerr << "    " << "-p p_min: peso minimo de las aristas\n";
		cerr << "    " << "-q p_max: peso maximo de las aristas\n";
		cerr << "    " << "-d: indica que el peso las aristas deben cumplir la"
		               << "desigualdad triangular\n";
		cerr << "    " << "-v v_min: valor minimo de los vertices\n";
		cerr << "    " << "-w v_max: valor maximo de los vertices";
		cerr << endl;
		return 1;
	}
}

	Grafo::size_type n = arg_n;
	Grafo::size_type m = arg_m;


	/*
	 * Genero un arbol de n vertices aleatoriamente
	 */

	Grafo G = Grafo(n);

{
	vector<Grafo::id_vertice> listos, pendientes;

	// Pongo a todos los vertices como pendientes
	for (auto its = G.vertices(); its.first != its.second; ++its.first) {
		pendientes.push_back(*its.first);
	}

	// Pongo un vertice como listo
	listos.push_back(pendientes.back());
	pendientes.pop_back();

	// Inicializo generadores de numeros aleatorios
	default_random_engine gen_arbol_lis(rd()), gen_arbol_pen(rd());

	// Agrego n-1 aristas de forma aleatoria formando un arbol
	for (size_t k = 1; k < n; ++k) {
		uniform_int_distribution<> unif_lis(0, listos.size()-1);
		uniform_int_distribution<> unif_pen(0, pendientes.size()-1);

		// Agrego una arista con un extremo en un vertice de listos y el otro
		// extremo en un vertice de pendientes
		size_t i = unif_lis(gen_arbol_lis);
		size_t j = unif_pen(gen_arbol_pen);
		assert(listos[i] != pendientes[j]);
		G.agregar_arista_rapido(listos[i], pendientes[j]);

		// Paso el vertice de pendientes a listos
		listos.push_back(pendientes[j]);
		pendientes.erase(pendientes.begin() + j);
	}
}


	/*
	 * Agrego aristas aleatoriamente hasta llegar a m aristas
	 */

{
	// Creo un vector con todos los vertices
	vector<Grafo::id_vertice> vertices;
	for (auto its = G.vertices(); its.first != its.second; ++its.first) {
		vertices.push_back(*its.first);
	}

	// Inicializo un generador de numeros aleatorios
	default_random_engine gen_vert(rd());
	uniform_int_distribution<> unif_vert(0, vertices.size()-1);

	// Agrego aristas aleatoriamente
	while (G.cant_aristas() < m) {
		// Obtengo dos vertices aleatorios
		Grafo::id_vertice u = vertices[unif_vert(gen_vert)];
		Grafo::id_vertice v = u;
		while (v == u) v = vertices[unif_vert(gen_vert)];

		// Agrego una arista entre los vertices
		assert(u != v);
		G.agregar_arista(u, v);
	}
}


	/*
	 * Genero los pesos de las aristas
	 */

	// Creo un mapa de pesos de las aristas
	struct arista {
		Grafo::id_vertice u, v;
		unsigned int p;
		arista(unsigned int p) : p(p) {}
	};

	vector<arista> pesos(m, UINT_MAX);

{
	// Inicializo un generador de numeros aleatorios
	default_random_engine gen_pesos(rd());
	uniform_int_distribution<> unif_pesos(arg_p_min, arg_p_max);

	for (auto its = G.aristas(); its.first != its.second; ++its.first) {
		// Obtengo los vertices sobre los que incide la arista
		auto u = its.first->vertices.first;
		auto v = its.first->vertices.second;

		// Genero un peso aleatorio
		unsigned int p = unif_pesos(gen_pesos);

		// Si se activo el flag, verifico que cumpla la desigualdad triangular
		if (arg_desig_trian) {

			// Busco los vertices adyacentes que tengan en comun

			// Obtengo los vertices adyacentes a ambos y la arista que realiza
			// la adyacencia
			typedef pair<Grafo::id_vertice, Grafo::id_arista> adyacencia;
			vector<adyacencia> ady_u, ady_v;

			for (auto its_u = G.adyacencias(u); its_u.first != its_u.second;
			++its_u.first) {
				ady_u.push_back(make_pair(G.adyacente(u, *its_u.first),
					its_u.first->id));
			}

			for (auto its_v = G.adyacencias(v); its_v.first != its_v.second;
			++its_v.first) {
				ady_v.push_back(make_pair(G.adyacente(v, *its_v.first),
					its_v.first->id));
			}

			// Obtengo los vertices adyacentes comunes a ambos
			vector<pair<Grafo::id_arista, Grafo::id_arista>> ady;

			auto comp = [](adyacencia &a, adyacencia &b)
				{ return a.first < b.first; };

			sort(ady_u.begin(), ady_u.end(), comp);
			sort(ady_v.begin(), ady_v.end(), comp);

			auto first1 = ady_u.begin(), last1 = ady_u.end();
			auto first2 = ady_v.begin(), last2 = ady_v.end();

			while (first1 != last1 && first2 != last2) {
				if (comp(*first1, *first2)) {
					++first1;
				} else {
					if (!comp(*first2, *first1)) {
						ady.emplace_back(first1->second, first2->second);
						++first1;
					}
					++first2;
				}
			}

			// Obtengo el rango en el cual se cumple la desigualdad triangular
			unsigned int cota_inf = 0;
			unsigned int cota_sup = UINT_MAX;
			bool acotado = false;

			for (auto it = ady.begin(); it != ady.end(); ++it) {
				auto e = it->first;
				auto f = it->second;

				if (pesos[e].p == UINT_MAX || pesos[f].p == UINT_MAX)
					continue;

				acotado = true;

				unsigned int p1 = pesos[e].p;
				unsigned int p2 = pesos[f].p;

				cota_inf = max(cota_inf, p1 >= p2 ? p1 - p2 : p2 - p1);
				cota_sup = min(cota_sup, p1 + p2);
			}

			if (acotado) {
				unsigned int p_min = max(cota_inf, arg_p_min);
				unsigned int p_max = min(cota_sup, arg_p_max);

				if (p_min > p_max) {
					cerr << "No se pudo generar un grafo que cumpla con la "
					     << "desigualdad triangular" << endl;
					exit(-1);
				}

				uniform_int_distribution<> unif_dt(p_min, p_max);
				p = unif_dt(gen_pesos);
			}
		}

		// Defino el peso
		if (u > v) swap(u, v);
		pesos[its.first->id].u = u;
		pesos[its.first->id].v = v;
		pesos[its.first->id].p = p;
	}
}


	/*
	 * Genero valores aleatorios para los vertices (si hace falta)
	 */

	// Creo un mapa de valores de los vertices
	vector<Grafo::id_vertice> valores(n);

	if (arg_valores) {
		// Inicializo un generador de numeros aleatorios
		default_random_engine gen_val(rd());
		uniform_int_distribution<> unif_val(arg_v_min, arg_v_max);

		// Genero los valores
		for (auto its = G.vertices(); its.first != its.second; ++its.first) {
			valores[*its.first] = unif_val(gen_val);
		}
	}


	/*
	 * Imprimo los datos generados
	 */

	cout << n << " " << m << endl;

	// Imprimo los valores de los vertices
	if (arg_valores) for (auto &v : valores) cout << v << endl;

	// Imprimo las aristas y sus pesos
	sort(pesos.begin(), pesos.end(), [](arista &e, arista &f) {
		return e.u < f.u || (e.u == f.u && e.v < f.v) ||
		       (e.u == f.u && e.v == f.v && e.p < f.p);
	});

	for (auto &a : pesos) cout << a.u << " " << a.v << " " << a.p << endl;


	return 0;
}