#ifndef __CRONOMETRO_HPP__
#define __CRONOMETRO_HPP__

#include <chrono>

class Cronometro {

	public:

		void iniciar() {
			start = std::chrono::steady_clock::now();
		}

		double mirar() {
			auto end = std::chrono::steady_clock::now() - start;
			return std::chrono::duration<double, std::milli>(end).count();
		}


	private:

		std::chrono::steady_clock::time_point start;

};

#endif