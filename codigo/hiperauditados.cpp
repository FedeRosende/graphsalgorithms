#include <algorithm>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <unistd.h>
#include <vector>
#include "cronometro.hpp"
#include "grafo.hpp"

using namespace std;

#define CAPACIDAD_TANQUE 60u


int arg_algoritmo = -1;
bool arg_tiempos = false;
bool arg_desig_trian = false;

void cm_dijkstra(const Grafo &G, const vector<unsigned int> &pesos,
vector<vector<unsigned int>> &distancias) {

	for (size_t i = 0; i < distancias.size(); ++i) {
		G.sp_dijkstra(pesos, distancias[i], i);
	}
}

void cm_dijkstra_pq(const Grafo &G, const vector<unsigned int> &pesos,
vector<vector<unsigned int>> &distancias) {

	for (size_t i = 0; i < distancias.size(); ++i) {
		G.sp_dijkstra_pq(pesos, distancias[i], i);
	}
}

void cm_a_estrella(const Grafo &G, const vector<unsigned int> &pesos,
vector<vector<unsigned int>> &distancias) {

	// Pongo como subestimacion de la distancia de ir de un vertice u otro
	// vertice v como el menor peso de las aristas que entran a v

	// Obtengo los menores pesos de las aristas que entran a cada vertice
	size_t n = G.cant_vertices();
	vector<unsigned int> subests_in(n, UINT_MAX);
	vector<unsigned int> subests_out(n, arg_desig_trian ? UINT_MAX : 0);

	for (auto its_v = G.vertices(); its_v.first != its_v.second; ++its_v.first)
	{
		auto u = *its_v.first;

		for (auto its_a = G.adyacencias(u); its_a.first != its_a.second;
		++its_a.first) {

			auto &a = *its_a.first;
			auto v = G.adyacente(u, a);
			subests_in[v] = min(subests_in[v], pesos[a.id]);
			subests_out[u] = min(subests_out[u], pesos[a.id]);
		}
	}

	// Defino la funcion heuristica
	auto heur = [&subests_in, &subests_out](const Grafo::id_vertice &u, const
	Grafo::id_vertice &v) {
		return u == v ? 0 : subests_out[u] + subests_in[v];
	};

	for (size_t i = 0; i < distancias.size(); ++i) {
		distancias[i][i] = 0;

		for (size_t j = i+1; j < distancias[i].size(); ++j) {
			distancias[i][j] = distancias[j][i] =
				G.sp_a_estrella(pesos, heur, i, j);
		}
	}
}

void cm_ford(const Grafo &G, const vector<unsigned int> &pesos,
vector<vector<unsigned int>> &distancias) {

	for (size_t i = 0; i < distancias.size(); ++i) {
		G.sp_ford(pesos, distancias[i], i);
	}
}

void cm_floyd(const Grafo &G, const vector<unsigned int> &pesos,
vector<vector<unsigned int>> &distancias) {

	G.sp_floyd(pesos, distancias);
}

void cm_dantzig(const Grafo &G, const vector<unsigned int> &pesos,
vector<vector<unsigned int>> &distancias) {

	G.sp_dantzig(pesos, distancias);
}

void (*camino_minimo[])(const Grafo &G, const vector<unsigned int> &pesos,
vector<vector<unsigned int>> &distancias) = {
	cm_dijkstra, cm_dijkstra_pq, cm_a_estrella, cm_ford, cm_floyd, cm_dantzig
};

unsigned int pd(vector<vector<vector<unsigned int>>> &sol,
const vector<unsigned int> &costos, const vector<vector<unsigned int>> &dist,
const size_t &u, const size_t &v, const unsigned int &l, const size_t &k) {

	// Tomo UINT_MAX como NIL y UINT_MAX - 1 como infinito

	auto &res = sol[u][l][k];
	if (res != UINT_MAX) return res;

	auto x = UINT_MAX - 1;

	// Caso base
	if (k == 0) {
		if (l < dist[u][v] && dist[u][v] <= CAPACIDAD_TANQUE) {
			x = costos[u] * (dist[u][v] - l);
		}
	}

	// Caso recursivo
	else {
		for (size_t w = 0; w < sol.size(); ++w) {
			if (dist[u][w] > CAPACIDAD_TANQUE || w == v) continue;

			unsigned int c, s;

			if (w == u) {
				c = 0;
				s = pd(sol, costos, dist, w, v, l, k-1);
			} else if (costos[u] >= costos[w]) {
				if (l >= dist[u][w]) continue;
				c = costos[u] * (dist[u][w] - l);
				s = pd(sol, costos, dist, w, v, 0, k-1);
			} else if (costos[u] < costos[w]) {
				c = costos[u] * (CAPACIDAD_TANQUE - l);
				s = pd(sol, costos, dist, w, v, CAPACIDAD_TANQUE - dist[u][w], k-1);
			}

			if (s != UINT_MAX - 1) x = min(x, c + s);
		}
	}

	res = x;

	return x;
}

int main(int argc, char *const *argv) {

	/*
	 * Parseo de argumentos
	 */

{
	opterr = 0; // evitar que getopt imprima mensajes de error
	bool error = false;
	int c;

	while ((c = getopt(argc, argv, "a:td")) != -1) {
		switch (c) {
			case 't':
				arg_tiempos = true;
				break;

			case 'd':
				arg_desig_trian = true;
				break;

			case 'a':
				try { arg_algoritmo = stoi(optarg); }
				catch (...) { error = true; }
				if (!error && (0 > arg_algoritmo || arg_algoritmo > 5))
					error = true;
				break;

			default:
				error = true;
		}
	}

	if (error || arg_algoritmo == -1) {
		cerr << "uso: " << argv[0] << " -a algoritmo [-d] [-t]\n";
		cerr << "    " << "-a algortimo: algoritmo a usar\n";
		cerr << "    " << "           " << "0  Dijkstra\n";
		cerr << "    " << "           " << "1  Dijkstra con Priority Queue\n";
		cerr << "    " << "           " << "2  A*\n";
		cerr << "    " << "           " << "3  Bellman-Ford\n";
		cerr << "    " << "           " << "4  Floyd-Warshall\n";
		cerr << "    " << "           " << "5  Dantzig\n";
		cerr << "    " << "-d: indica que la entrada corresponde a un grafo "
		               << "que cumple con la desigualdad triangular (solo "
		               << "para A*)\n";
		cerr << "    " << "-t: imprimir tiempo de ejecucion (en milisegundos) "
		               << "por error estandar";
		cerr << endl;
		return 1;
	}
}

	/*
	 * Lectura de datos por entrada estandar
	 */

	// Leo n y m
	size_t n, m;
	cin >> n;
	cin >> m;

	// Leo los costos de combustible de cada ciudad
	vector<unsigned int> costos(n);
	for (unsigned int i = 0; i < n; ++i) cin >> costos[i];

	// Leo las rutas
	struct ruta { unsigned int a, b, l; };
	vector<ruta> rutas(m);
	for (unsigned int i = 0; i < m; ++i) {
		cin >> rutas[i].a;
		cin >> rutas[i].b;
		cin >> rutas[i].l;
	}


	/*
	 * Resolucion del problema
	 */

	// Creo un cronometro y empiezo a medir el tiempo
	Cronometro crono;
	crono.iniciar();

	// Creo un grafo en donde los vertices son ciudades, las aristas son rutas
	// y los pesos de las aristas son las cantidades de litros
	Grafo G(n);
	vector<unsigned int> litros_rutas(m);
	for (auto &r : rutas) litros_rutas[G.agregar_arista_rapido(r.a,r.b)] = r.l;

	// Obtengo la cantidad minima de litros necesaria para ir entre cada ciudad
	vector<vector<unsigned int>> min_litros(n, vector<unsigned int>(n));
	camino_minimo[arg_algoritmo](G, litros_rutas, min_litros);

	// Obtengo los costos minimos con programacion dinamica

	// Creo un mapa de costos minimos
	// min_costos[j][i] es el costo minimo de ir a (j+1) desde i
	vector<vector<unsigned int>> min_costos(n-1);
	for (size_t j = 0; j < min_costos.size(); ++j) min_costos[j].resize(j+1);

	for (size_t j = 0; j < min_costos.size(); ++j) {
		vector<vector<vector<unsigned int>>> sol(n,
			vector<vector<unsigned int>>(CAPACIDAD_TANQUE,
				vector<unsigned int>(2*n, UINT_MAX))); // tomo UINT_MAX como NIL

		for (size_t i = 0; i <= j; ++i) {
			min_costos[j][i] = pd(sol, costos, min_litros, i, j+1, 0, 2*n-1);
		}
	}

	// Miro el cronometro
	auto tiempo = crono.mirar();


	/*
	 * Impresion de la solucion
	 */

	for (size_t i = 0; i < n; ++i) {
		for (size_t j = i+1; j < n; ++j) {
			cout << i << " " << j << " " << min_costos[j-1][i] << endl;
		}
	}

	if (arg_tiempos) cerr << tiempo << endl;


	return 0;
}