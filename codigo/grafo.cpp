#include <algorithm>
#include "grafo.hpp"


/*
 * Grafo
 */

Grafo::Grafo(bool dirigido) : _dirigido(dirigido) {}

Grafo::Grafo(const size_type &n, bool dirigido)
: _dirigido(dirigido), _adyacencias(n) {}

Grafo::Grafo(const Grafo &otro) {
	_dirigido = otro._dirigido;
	_aristas = otro._aristas;
	_adyacencias = otro._adyacencias;
}

Grafo::~Grafo() {}

void Grafo::swap(Grafo &otro) {
	std::swap(_dirigido, otro._dirigido);
	std::swap(_aristas, otro._aristas);
	std::swap(_adyacencias, otro._adyacencias);
}

Grafo& Grafo::operator=(Grafo otro) {
	swap(otro);
	return *this;
}

Grafo::size_type Grafo::cant_vertices() const {
	return _adyacencias.size();
}

Grafo::size_type Grafo::cant_aristas() const {
	return _aristas.size();
}

Grafo::id_vertice Grafo::agregar_vertice() {
	id_vertice id = cant_vertices();
	agregar_vertices(1);
	return id;
}

void Grafo::agregar_vertices(const size_type &n) {
	_adyacencias.resize(_adyacencias.size() + n);
}

std::pair<Grafo::id_arista, bool> Grafo::agregar_arista(id_vertice u,
id_vertice v) {

	std::pair<Grafo::id_arista, bool> ret;

	Arista arista(0, u, v);

	auto it = _aristas.begin();
	while (it != _aristas.end() && *it != arista) ++it;

	if (it == _aristas.end()) {
		ret.first = agregar_arista_rapido(u, v);
		ret.second = true;
	} else {
		ret.first = it - _aristas.begin();
		ret.second = false;
	}

	return ret;
}

Grafo::id_arista Grafo::agregar_arista_rapido(id_vertice u, id_vertice v) {
	id_arista id = _aristas.size();
	_aristas.push_back(Arista(id, u, v));
	_adyacencias[u].push_back(id);
	if (!_dirigido) _adyacencias[v].push_back(id);
	return id;
}

const Grafo::Arista& Grafo::arista(const id_arista &id) const {
	return _aristas[id];
}

Grafo::iteradores_vertices Grafo::vertices() const {
	return std::make_pair(iterador_vertices(this, _adyacencias.begin()),
		                  iterador_vertices(this, _adyacencias.end()));
}

Grafo::iteradores_aristas Grafo::aristas() const {
	return std::make_pair(_aristas.cbegin(), _aristas.cend());
}

Grafo::iteradores_adyacencias Grafo::adyacencias(const id_arista &id) const {
	return std::make_pair(iterador_adyacencias(this, _adyacencias[id].begin()),
		                  iterador_adyacencias(this, _adyacencias[id].end()));
}

Grafo::id_vertice Grafo::adyacente(const id_vertice &v, const Arista &a) const{
	return v != a.vertices.first ? a.vertices.first : a.vertices.second;
}


/*
 * Grafo::iterador_vertices
 */

Grafo::iterador_vertices::iterador_vertices(const Grafo *G,
	adyacencias_type::const_iterator it) : _grafo(G), _it(it) {}

Grafo::id_vertice Grafo::iterador_vertices::operator*() const {
	return _it - _grafo->_adyacencias.begin();
}

Grafo::iterador_vertices& Grafo::iterador_vertices::operator++() {
	++_it;
	return *this;
}

Grafo::iterador_vertices& Grafo::iterador_vertices::operator--() {
	--_it;
	return *this;
}

Grafo::iterador_vertices Grafo::iterador_vertices::operator++(int) {
	auto ret = *this;
	++_it;
	return ret;
}

Grafo::iterador_vertices Grafo::iterador_vertices::operator--(int) {
	auto ret = *this;
	--_it;
	return ret;
}

bool Grafo::iterador_vertices::operator==(const iterador_vertices &otro)
const {
	return _grafo == otro._grafo && _it == otro._it;
}

bool Grafo::iterador_vertices::operator!=(const iterador_vertices &otro)
const {
	return !operator==(otro);
}


/*
 * Grafo::iterador_adyacencias
 */

Grafo::iterador_adyacencias::iterador_adyacencias(const Grafo *G,
	adyacencia_type::const_iterator it) : _grafo(G), _it(it) {}

const Grafo::Arista& Grafo::iterador_adyacencias::operator*() const {
	return _grafo->_aristas[*_it];
}

const Grafo::Arista* Grafo::iterador_adyacencias::operator->() const {
	return &operator*();
}

Grafo::iterador_adyacencias& Grafo::iterador_adyacencias::operator++() {
	++_it;
	return *this;
}

Grafo::iterador_adyacencias& Grafo::iterador_adyacencias::operator--() {
	--_it;
	return *this;
}

Grafo::iterador_adyacencias Grafo::iterador_adyacencias::operator++(int) {
	auto ret = *this;
	++_it;
	return ret;
}

Grafo::iterador_adyacencias Grafo::iterador_adyacencias::operator--(int) {
	auto ret = *this;
	--_it;
	return ret;
}

bool Grafo::iterador_adyacencias::operator==(const iterador_adyacencias &otro)
const {
	return _grafo == otro._grafo && _it == otro._it;
}

bool Grafo::iterador_adyacencias::operator!=(const iterador_adyacencias &otro)
const {
	return !operator==(otro);
}


/*
 * Grafo::Arista
 */

Grafo::Arista::Arista(const id_arista id, const id_vertice u,
                      const id_vertice v) : id(id), vertices(u, v) {}

bool Grafo::Arista::operator==(const Arista &otro) const {
	return (vertices.first == otro.vertices.first &&
	        vertices.second == otro.vertices.second) ||
	       (vertices.first == otro.vertices.second &&
	       	vertices.second == otro.vertices.first);
}

bool Grafo::Arista::operator!=(const Arista &otro) const {
	return !operator==(otro);
}