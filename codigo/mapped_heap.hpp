#ifndef __MAPPED_HEAP_HPP__
#define __MAPPED_HEAP_HPP__

#include <algorithm>
#include <functional>

template<
	class RandomIt,
	class Map,
	class Compare = std::less<typename RandomIt::value_type>
>
class mapped_heap {

	public:

		using value_type = typename RandomIt::value_type;

		mapped_heap(RandomIt first, RandomIt last, Map *map,
		Compare lt = Compare())
			: _first(first), _last(last), _map(map), _lt(lt) {}

		void make() const {
			if (_first == _last) return;
			RandomIt it = _last;
			do _sift_down(--it);
			while (it != _first);
		}

		value_type top() const { return *_first; }

		void pop() {
			_swap_it(_first, --_last);
			_sift_down(_first);
		}

		void push() { _sift_up(_last++); }

		void update(const RandomIt &it) {
			if (_has_parent(it) && _lt(*it, *_parent(it))) _sift_down(it);
			else _sift_up(it);
		}

		size_t size() const { return _container_size(); };


	private:

		RandomIt _first;
		RandomIt _last;
		Map *_map;
		Compare _lt;

		void _swap_it(RandomIt a, RandomIt b) const {
			std::swap((*_map)[*a], (*_map)[*b]);
			std::iter_swap(a, b);
		}

		size_t _it_index(const RandomIt &it) const {
			return std::distance(_first, it);
		}

		size_t _parent_index(const RandomIt &it) const {
			return (_it_index(it) - 1) / 2;
		}

		size_t _left_child_index(const RandomIt &it) const {
			return 2 * _it_index(it) + 1;
		}

		size_t _right_child_index(const RandomIt &it) const {
			return _left_child_index(it) + 1;
		}

		RandomIt _parent(const RandomIt &it) const {
			return _first + _parent_index(it);
		}

		RandomIt _left_child(const RandomIt &it) const {
			return _first + _left_child_index(it);
		}

		RandomIt _right_child(const RandomIt &it) const {
			return _first + _right_child_index(it);
		}

		size_t _container_size() const {
			return std::distance(_first, _last);
		}

		bool _has_parent(const RandomIt &it) const {
			return _it_index(it) > 0;
		}

		bool _has_left_child(const RandomIt &it) const {
			return _left_child_index(it) < _container_size();
		}

		bool _has_right_child(const RandomIt &it) const {
			return _right_child_index(it) < _container_size();
		}

		void _sift_down(const RandomIt &it) const {
			RandomIt current = it;

			while (_has_left_child(current)) {
				RandomIt left = _left_child(current);
				RandomIt largest = _lt(*current, *left) ? left : current;

				if (_has_right_child(current)) {
					RandomIt right = _right_child(current);
					if (_lt(*largest, *right)) largest = right;
				}

				if (current == largest) break;

				_swap_it(current, largest);
				current = largest;
			}
		}

		void _sift_up(const RandomIt &it) const {
			RandomIt current = it;
			RandomIt parent;
			while (current != _first &&
			!_lt(*current, *(parent = _parent(current)))) {
				_swap_it(current, parent);
				current = parent;
			}
		}

};


#endif