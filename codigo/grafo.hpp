#ifndef __GRAFO_HPP__
#define __GRAFO_HPP__

#include <algorithm>
#include <list>
#include <vector>
#include "mapped_heap.hpp"
#include "uint_disjoint_set.hpp"

#define UINT_MAX ((unsigned int) -1)


class Grafo {

	struct Arista;

	public:

		class iterador_vertices;
		class iterador_adyacencias;
		friend iterador_vertices;
		friend iterador_adyacencias;

		using size_type = size_t;
		using id_vertice = size_type;
		using id_arista = size_type;
		using aristas_type = std::vector<Arista>;
		using adyacencia_type = std::list<id_arista>;
		using adyacencias_type = std::vector<adyacencia_type>;
		using iterador_aristas = aristas_type::const_iterator;
		using iteradores_vertices = std::pair<iterador_vertices,
		                                      iterador_vertices>;
		using iteradores_aristas = std::pair<iterador_aristas,
		                                     iterador_aristas>;
		using iteradores_adyacencias = std::pair<iterador_adyacencias,
		                                         iterador_adyacencias>;

		Grafo(bool dirigido = false);
		Grafo(const size_type &n, bool dirigido = false);
		Grafo(const Grafo &otro);
		~Grafo();

		void swap(Grafo &otro);
		Grafo& operator=(Grafo otro);

		size_type cant_vertices() const;
		size_type cant_aristas() const;
		id_vertice agregar_vertice();
		void agregar_vertices(const size_type &n);
		std::pair<id_arista, bool> agregar_arista(id_vertice u, id_vertice v);
		id_arista agregar_arista_rapido(id_vertice u, id_vertice v);
		const Arista& arista(const id_arista &id) const;

		iteradores_vertices vertices() const;
		iteradores_aristas aristas() const;
		iteradores_adyacencias adyacencias(const id_arista &id) const;
		id_vertice adyacente(const id_vertice &v, const Arista &a) const;

		template<class MapaPesos, class InsertIt>
		void agm_prim(const MapaPesos &pesos, InsertIt ins_it) const;

		template<class MapaPesos, class InsertIt>
		void agm_kruskal(const MapaPesos &pesos, InsertIt ins_it) const;

		template<class MapaPesos, class InsertIt>
		void agm_kruskal_path_comp(const MapaPesos &pesos, InsertIt ins_it)
		const;

		template<class MapaPesos, class MapaDistancias>
		void sp_dijkstra(const MapaPesos &pesos, MapaDistancias &distancias,
		const id_vertice &v0) const;

		template<class MapaPesos, class MapaDistancias>
		void sp_dijkstra_pq(const MapaPesos &pesos, MapaDistancias &distancias,
		const id_vertice &v0) const;

		template<class MapaPesos, class FunHeuristica>
		unsigned int sp_a_estrella(const MapaPesos &pesos, const FunHeuristica
		&h, const id_vertice &v0, const id_vertice &vf) const;

		template<class MapaPesos, class MapaDistancias>
		void sp_ford(const MapaPesos &pesos, MapaDistancias &distancias,
		const id_vertice &v0) const;

		template<class MapaPesos, class MapaDistancias>
		void sp_floyd(const MapaPesos &pesos, MapaDistancias &distancias)
		const;

		template<class MapaPesos, class MapaDistancias>
		void sp_dantzig(const MapaPesos &pesos, MapaDistancias &distancias)
		const;

	private:

		bool _dirigido;
		aristas_type _aristas;
		adyacencias_type _adyacencias;

};


class Grafo::iterador_vertices {
	public:
		id_vertice operator*() const;
		iterador_vertices& operator++();
		iterador_vertices& operator--();
		iterador_vertices operator++(int);
		iterador_vertices operator--(int);
		bool operator==(const iterador_vertices &otro) const;
		bool operator!=(const iterador_vertices &otro) const;

	private:
		iterador_vertices(const Grafo *G, adyacencias_type::const_iterator it);
		const Grafo *_grafo;
		adyacencias_type::const_iterator _it;

	friend Grafo;
};


class Grafo::iterador_adyacencias {
	public:
		const Arista& operator*() const;
		const Arista* operator->() const;
		iterador_adyacencias& operator++();
		iterador_adyacencias& operator--();
		iterador_adyacencias operator++(int);
		iterador_adyacencias operator--(int);
		bool operator==(const iterador_adyacencias &otro) const;
		bool operator!=(const iterador_adyacencias &otro) const;

	private:
		iterador_adyacencias(const Grafo *G,
			                 adyacencia_type::const_iterator it);
		const Grafo *_grafo;
		adyacencia_type::const_iterator _it;

	friend Grafo;
};


struct Grafo::Arista {
	id_arista id;
	std::pair<id_vertice, id_vertice> vertices;

	Arista(const id_arista id, const id_vertice u, const id_vertice v);
	bool operator==(const Arista &otro) const;
	bool operator!=(const Arista &otro) const;
};


/*
 * Definiciones de function templates
 */

template<class MapaPesos, class InsertIt>
void Grafo::agm_prim(const MapaPesos &pesos, InsertIt ins_it) const {

	unsigned int n = cant_vertices();

	// Creo un mapa que indica si un vertice fue visitado
	std::vector<bool> visitado(n, false);

	// Creo un mapa que guarda la distancia minima de todos los vertices a
	// algun vertice del arbol generado hasta el momento.
	// Inicializo las distancias en el valor maximo posible.
	std::vector<unsigned int> distancias(n, UINT_MAX);

	// Creo un mapa que guarda un puntero a la arista que realiza la distancia
	// minima para cada vertice.
	// Inicializo las aristas en un puntero nulo.
	std::vector<const Arista*> aristas(n, nullptr);

	// Uso un min-heap de vertices en funcion de sus distancias.
	// Para poder obtener la posicion en el heap de un vertice en O(1) uso el
	// tipo mapped_heap que implementa un heap que actualiza un mapa de
	// posiciones de los vertices. Es decir, cada vez que intercambia dos
	// elementos en el heap intercambia los valores correspondientes en el mapa
	// de posiciones

	std::vector<id_vertice> vertices(n);
	std::vector<id_vertice> indices(n);

	for (size_t i = 0; i < n; ++i) vertices[i] = indices[i] = i;

	struct Compare { // functor de comparacion por distancia
		std::vector<unsigned int> *distancias;
		Compare(std::vector<unsigned int> *d) : distancias(d) {}
		bool operator()(const id_vertice &u, const id_vertice &v) const
			{ return (*distancias)[u] > (*distancias)[v]; }
	};

	mapped_heap<
		std::vector<id_vertice>::iterator,
		std::vector<id_vertice>,
		Compare
	> heap(vertices.begin(), vertices.end(), &indices, Compare(&distancias));

	heap.make(); // heapify

	// Genero un AGM
	while (heap.size() > 0) {
		// Obtengo el vertice de menor distancia al arbol generado
		id_vertice u = heap.top();
		heap.pop();

		// Lo marco como visitado
		visitado[u] = true;

		// Agrego la arista que realiza el minimo si es valida
		if (aristas[u] != nullptr) *(ins_it++) = aristas[u]->id;

		// Actualizo las distancias minimas hacia los vertices adyacentes a u y
		// los punteros a las aristas que realizan las distancias minimas
		for (auto its = adyacencias(u); its.first != its.second; ++its.first) {
			id_vertice v = adyacente(u, *its.first);

			if (!visitado[v] && distancias[v] > pesos[its.first->id]) {
				distancias[v] = pesos[its.first->id];
				aristas[v] = &(*its.first);
				// Restablezco el invariante de heap
				heap.update(vertices.begin() + indices[v]);
			}
		}
	}
}

template<class MapaPesos, class InsertIt>
void Grafo::agm_kruskal(const MapaPesos &pesos, InsertIt ins_it) const {

	unsigned int n = cant_vertices();

	// Obtengo todas las aristas del grafo
	std::vector<id_arista> aristas(cant_aristas());
	{
	size_t i = 0;
	for (auto its = this->aristas(); its.first != its.second; ++its.first)
		aristas[i++] = its.first->id;
	}

	// Ordeno las aristas por peso
	auto cmp = [&pesos](id_arista a, id_arista b)
		{ return pesos[a] < pesos[b]; };
	make_heap(aristas.begin(), aristas.end(), cmp);
	sort_heap(aristas.begin(), aristas.end(), cmp);

	// Creo un mapa que indica la componente a la que pertenece cada vertice
	std::vector<unsigned int> componentes(n);
	for (size_t i = 0; i < n; ++i) componentes[i] = i;

	// Genero un AGM
	auto it = aristas.begin();
	unsigned int cuenta = 0; // cuenta la cantidad de aristas agregadas

	while (cuenta < n-1) {
		// Obtengo los vertices sobre los que incide la arista
		id_vertice u = arista(*it).vertices.first;
		id_vertice v = arista(*it).vertices.second;

		// Agrego la arista si no forma un ciclo (si no conecta dos vertices de
		// la misma componente) y actualizo las componentes
		if (componentes[u] != componentes[v]) {
			*(ins_it++) = *it;
			unsigned int comp_v = componentes[v];
			for (auto &c : componentes) if (c == comp_v) c = componentes[u];
			++cuenta;
		}

		++it;
	}
}

template<class MapaPesos, class InsertIt>
void Grafo::agm_kruskal_path_comp(const MapaPesos &pesos, InsertIt ins_it)
const {

	unsigned int n = cant_vertices();

	// Obtengo todas las aristas del grafo
	std::vector<id_arista> aristas(cant_aristas());
	{
	size_t i = 0;
	for (auto its = this->aristas(); its.first != its.second; ++its.first)
		aristas[i++] = its.first->id;
	}

	// Ordeno las aristas por peso
	auto cmp = [&pesos](id_arista a, id_arista b)
		{ return pesos[a] < pesos[b]; };
	make_heap(aristas.begin(), aristas.end(), cmp);
	sort_heap(aristas.begin(), aristas.end(), cmp);

	// Creo un disjoint-set que indica la componente a la que pertenece cada
	// vertice
	uint_disjoint_set componentes(n);

	// Genero un AGM
	auto it = aristas.begin();

	while (componentes.sets() > 1) {
		// Obtengo los vertices sobre los que incide la arista
		id_vertice u = arista(*it).vertices.first;
		id_vertice v = arista(*it).vertices.second;

		// Agrego la arista si no forma un ciclo (si no conecta dos vertices de
		// la misma componente) y actualizo las componentes
		if (componentes.find(u) != componentes.find(v)) {
			componentes.merge(u, v);
			*(ins_it++) = *it;
		}

		++it;
	}
}

template<class MapaPesos, class MapaDistancias>
void Grafo::sp_dijkstra(const MapaPesos &pesos, MapaDistancias &distancias,
const id_vertice &v0) const {

	unsigned int n = cant_vertices();

	// Creo un mapa que indica si ya se termino de procesar un vertice
	std::vector<bool> listo(n, false);

	// Inicializo las distancias de los vertices
	std::fill(distancias.begin(), distancias.end(), UINT_MAX);
	distancias[v0] = 0;

	// Calculo las distancias minimas
	unsigned int cuenta = 0; // cantidad de vertices listos
	while (cuenta < n) {
		// Obtengo el vertice de menor distancia
		id_vertice u;
		unsigned int min_dist = UINT_MAX;
		for (auto its = vertices(); its.first != its.second; ++its.first) {
			id_vertice v = *its.first;

			if (!listo[v] && distancias[v] < min_dist) {
				u = v;
				min_dist = distancias[v];
			}
		}

		// Lo marco como terminado
		listo[u] = true;
		++cuenta;

		// Si la distancia de u es infinito no actualizo nada
		if (distancias[u] == UINT_MAX) continue;

		// Actualizo las distancias minimas hacia los vertices adyacentes a u y
		// los punteros a las aristas que realizan las distancias minimas
		for (auto its = adyacencias(u); its.first != its.second; ++its.first) {
			id_vertice v = adyacente(u, *its.first);
			if (listo[v]) continue;

			auto nueva_dist = pesos[its.first->id] + distancias[u];

			if (distancias[v] > nueva_dist) {
				distancias[v] = nueva_dist;
			}
		}
	}
}

template<class MapaPesos, class MapaDistancias>
void Grafo::sp_dijkstra_pq(const MapaPesos &pesos, MapaDistancias &distancias,
const id_vertice &v0) const {

	unsigned int n = cant_vertices();

	// Creo un mapa que indica si ya se termino de procesar un vertice
	std::vector<bool> listo(n, false);

	// Uso un min-heap de vertices en funcion de sus distancias.
	// Para poder obtener la posicion en el heap de un vertice en O(1) uso el
	// tipo mapped_heap que implementa un heap que actualiza un mapa de
	// posiciones de los vertices. Es decir, cada vez que intercambia dos
	// elementos en el heap intercambia los valores correspondientes en el mapa
	// de posiciones

	std::vector<id_vertice> vertices(n);
	std::vector<id_vertice> indices(n);

	for (size_t i = 0; i < n; ++i) vertices[i] = indices[i] = i;

	struct Compare { // functor de comparacion por distancia
		const MapaDistancias &distancias;
		Compare(const std::vector<unsigned int> &d) : distancias(d) {}
		bool operator()(const id_vertice &u, const id_vertice &v) const
			{ return distancias[u] > distancias[v]; }
	};

	mapped_heap<
		std::vector<id_vertice>::iterator,
		std::vector<id_vertice>,
		Compare
	> heap(vertices.begin(), vertices.end(), &indices, Compare(distancias));

	// Inicializo las distancias de los vertices
	std::fill(distancias.begin(), distancias.end(), UINT_MAX);
	distancias[v0] = 0;

	heap.make(); // heapify

	// Calculo las distancias minimas
	while (heap.size() > 0) {
		// Obtengo el vertice de menor distancia
		id_vertice u = heap.top();
		heap.pop();

		// Lo marco como terminado
		listo[u] = true;

		// Si la distancia de u es infinito no actualizo nada
		if (distancias[u] == UINT_MAX) continue;

		// Actualizo las distancias minimas hacia los vertices adyacentes a u y
		// los punteros a las aristas que realizan las distancias minimas
		for (auto its = adyacencias(u); its.first != its.second; ++its.first) {
			id_vertice v = adyacente(u, *its.first);
			if (listo[v]) continue;

			auto nueva_dist = pesos[its.first->id] + distancias[u];

			if (distancias[v] > nueva_dist) {
				distancias[v] = nueva_dist;
				// Restablezco el invariante de heap
				heap.update(vertices.begin() + indices[v]);
			}
		}
	}
}

template<class MapaPesos, class FunHeuristica>
unsigned int Grafo::sp_a_estrella(const MapaPesos &pesos,
const FunHeuristica &h, const id_vertice &v0, const id_vertice &vf) const {

	unsigned int n = cant_vertices();

	// Creo un mapa que indica si ya se termino de procesar un vertice
	std::vector<bool> listo(n, false);

	// Creo un mapa de distancias
	std::vector<unsigned int> distancias(n);

	// Uso un min-heap de vertices en funcion de sus distancias.
	// Para poder obtener la posicion en el heap de un vertice en O(1) uso el
	// tipo mapped_heap que implementa un heap que actualiza un mapa de
	// posiciones de los vertices. Es decir, cada vez que intercambia dos
	// elementos en el heap intercambia los valores correspondientes en el mapa
	// de posiciones

	std::vector<id_vertice> vertices(n);
	std::vector<id_vertice> indices(n);

	for (size_t i = 0; i < n; ++i) vertices[i] = indices[i] = i;

	struct Compare { // functor de comparacion
		std::vector<unsigned int> &distancias;
		const FunHeuristica &h;
		const id_vertice &vf;
		Compare(std::vector<unsigned int> &d, const FunHeuristica &h,
		const id_vertice &vf) : distancias(d), h(h), vf(vf) {}
		bool operator()(const id_vertice &u, const id_vertice &v) const {
			if (distancias[u] == UINT_MAX) return true;
			if (distancias[v] == UINT_MAX) return false;
			return distancias[u] + h(u, vf) > distancias[v] + h(v, vf);
		}
	};

	mapped_heap<
		std::vector<id_vertice>::iterator,
		std::vector<id_vertice>,
		Compare
	> heap(vertices.begin(), vertices.end(), &indices,
	       Compare(distancias, h, vf));

	// Inicializo las distancias de los vertices
	std::fill(distancias.begin(), distancias.end(), UINT_MAX);
	distancias[v0] = 0;

	heap.make(); // heapify

	// Calculo las distancias minimas
	while (heap.size() > 0) {
		// Obtengo el vertice de menor distancia
		id_vertice u = heap.top();

		// Si es el vértice final, termino
		if (u == vf) break;

		heap.pop();

		// Lo marco como terminado
		listo[u] = true;

		// Si la distancia de u es infinito no actualizo nada
		if (distancias[u] == UINT_MAX) continue;

		// Actualizo las distancias minimas hacia los vertices adyacentes a u y
		// los punteros a las aristas que realizan las distancias minimas
		for (auto its = adyacencias(u); its.first != its.second; ++its.first) {
			id_vertice v = adyacente(u, *its.first);
			if (listo[v]) continue;

			auto nueva_dist = pesos[its.first->id] + distancias[u];

			if (distancias[v] > nueva_dist) {
				distancias[v] = nueva_dist;
				// Restablezco el invariante de heap
				heap.update(vertices.begin() + indices[v]);
			}
		}
	}

	return distancias[vf];
}

template<class MapaPesos, class MapaDistancias>
void Grafo::sp_ford(const MapaPesos &pesos, MapaDistancias &distancias,
const id_vertice &v0) const {

		// Inicializo las distancias de los vertices
		std::fill(distancias.begin(), distancias.end(), UINT_MAX);
		distancias[v0] = 0;

		// Creo una variable que indica si cambiaron las distancias
		bool hay_cambios = true;

		while (hay_cambios) {

			hay_cambios = false;

			// Actualizo las distancias
			for (auto its = vertices(); its.first != its.second; ++its.first) {
				id_vertice u = *its.first;

				for (auto its = adyacencias(u); its.first != its.second;
				++its.first) {

					id_vertice v = adyacente(u, *its.first);
					if (distancias[u] == UINT_MAX) continue;

					auto nueva_dist = distancias[u] + pesos[its.first->id];

					if (distancias[v] > nueva_dist) {
						distancias[v] = nueva_dist;
						hay_cambios = true;
					}
				}
			}
		}
}

template<class MapaPesos, class MapaDistancias>
void Grafo::sp_floyd(const MapaPesos &pesos, MapaDistancias &distancias)
const {

	// Inicializo las distancias
	for (unsigned int i = 0; i < distancias.size(); i++) {
		std::fill(distancias[i].begin(), distancias[i].end(), UINT_MAX);
	}

	for (auto its_v = vertices(); its_v.first != its_v.second; ++its_v.first) {
		id_vertice v = *its_v.first;
		distancias[v][v] = 0;

		for (auto its_a = adyacencias(v); its_a.first != its_a.second;
		++its_a.first) {
			id_vertice w = adyacente(v, *its_a.first);
			distancias[v][w] = pesos[its_a.first->id];
		}
	}

	// Obtengo las distancias minimas
	for (auto its_k = vertices(); its_k.first != its_k.second; ++its_k.first) {
		id_vertice k = *its_k.first;

		for (auto its_i = vertices(); its_i.first != its_i.second;
		++its_i.first) {

			id_vertice i = *its_i.first;

			if (i != k && distancias[i][k] != UINT_MAX) {

				for (auto its_j = vertices(); its_j.first != its_j.second;
				++its_j.first) {

					id_vertice j = *its_j.first;

					if (distancias[k][j] == UINT_MAX) continue;

					distancias[i][j] = std::min(distancias[i][j],
						distancias[i][k] + distancias[k][j]);
				}
			}
		}
	}
}

template<class MapaPesos, class MapaDistancias>
void Grafo::sp_dantzig(const MapaPesos &pesos, MapaDistancias &distancias)
const {

	// Inicializo las distancias
	for (unsigned int i = 0; i < distancias.size(); i++) {
		std::fill(distancias[i].begin(), distancias[i].end(), UINT_MAX);
	}

	for (auto its_v = vertices(); its_v.first != its_v.second; ++its_v.first) {
		id_vertice v = *its_v.first;
		distancias[v][v] = 0;

		for (auto its_a = adyacencias(v); its_a.first != its_a.second;
		++its_a.first) {
			id_vertice w = adyacente(v, *its_a.first);
			distancias[v][w] = pesos[its_a.first->id];
		}
	}

	// Obtengo las distancias minimas
	for (auto its_k = vertices(); its_k.first != its_k.second; ++its_k.first) {
		id_vertice k = *its_k.first;

		// Actualizo los bordes de la matriz
		for (auto its_i = vertices(); its_i.first != its_k.first;
		++its_i.first) {

			id_vertice i = *its_i.first;

			for (auto its_j = vertices(); its_j.first != its_k.first;
			++its_j.first) {

				id_vertice j = *its_j.first;

				if (distancias[i][j] != UINT_MAX &&
					distancias[j][k] != UINT_MAX) {

					distancias[i][k] = std::min(distancias[i][k],
						distancias[i][j] + distancias[j][k]);
				}

				if (distancias[k][j] != UINT_MAX &&
					distancias[j][i] != UINT_MAX) {

					distancias[k][i] = std::min(distancias[k][i],
						distancias[k][j] + distancias[j][i]);
				}
			}
		}

		// Actualizo el interior de la matriz
		for (auto its_i = vertices(); its_i.first != its_k.first;
		++its_i.first) {

			id_vertice i = *its_i.first;

			for (auto its_j = vertices(); its_j.first != its_k.first;
			++its_j.first) {

				id_vertice j = *its_j.first;

				if (distancias[i][k] != UINT_MAX &&
					distancias[k][j] != UINT_MAX) {

					distancias[i][j] = std::min(distancias[i][j],
						distancias[i][k] + distancias[k][j]);
				}
			}
		}
	}
}

#endif